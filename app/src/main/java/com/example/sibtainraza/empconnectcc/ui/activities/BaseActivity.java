//package com.example.sibtainraza.empconnectcc.ui.activities;
//
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//
//import com.example.sibtainraza.empconnectcc.network.iAppServices;
//
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//
///**
// * Created by Sibtain Raza on 12/23/2016.
// */
//public class BaseActivity extends AppCompatActivity {
//
//    private SharedPreferences pref;
//    private SharedPreferences.Editor prefEditor;
//
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        pref = getApplicationContext().getSharedPreferences("Access Token", MODE_PRIVATE);
//        prefEditor = pref.edit();
//    }
//
//
//    protected void setSharedPref(String key, String value) {
//        prefEditor.putString(key, value);
//        prefEditor.commit();
//    }
//
//    protected String getSharedPref(String key) {
//        return pref.getString(key, "");
//    }
//
//    protected void removeSharedPref(String key) {
//        prefEditor.remove(key);
//        prefEditor.commit();
//    }
//
//
//
//}

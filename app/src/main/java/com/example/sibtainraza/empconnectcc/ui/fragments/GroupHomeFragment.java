package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.GroupMemberAdapter;
import com.example.sibtainraza.empconnectcc.adapters.UserAdapter;
import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.utils.AppLog;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnGroupHomeFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupHomeFragment#newInstance} factory method to
 * create an instance of this fragment. Changes
 */
public class GroupHomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TextView textViewGroupTitle,textViewGroupDesc,textViewFenceName;
    Button btnGroupMembers;
    ImageButton btnGroupChat, btnGroupTask;
    FloatingActionButton fabGroupSetting;
    ListView groupMembers;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Group groupObj;

    private View rootView;
    private OnGroupHomeFragmentInteractionListener mListener;

    public GroupHomeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static GroupHomeFragment newInstance(Group group) {
        GroupHomeFragment fragment = new GroupHomeFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1,group);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupObj = getArguments().getParcelable(ARG_PARAM1);
            AppLog.d("GroupObject - ",groupObj.getGroupTitle()+"");
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_group_home, container, false);

//        GroupTaskFragment fragment = new GroupTaskFragment();
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
////        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
//        fragmentTransaction.replace(R.id.frameGroupTask, fragment);
//        fragmentTransaction.commit();

        initViews();
        setListeners();
        setAdapters();

        textViewGroupTitle.setText(groupObj.getGroupTitle().toString().trim());
        textViewFenceName.setText(groupObj.getGroupFenceName().toString().trim());
        textViewGroupDesc.setText(groupObj.getGroupDesc().toString().trim());

        return rootView;
    }

    private void setAdapters() {
        groupMembers.setAdapter(new GroupMemberAdapter(getContext()));
    }

    private void initViews() {
//        btnGroupMembers = (Button) rootView.findViewById(R.id.gHomeBtn1);



        //groupTitlegroupFenceLocationgroupDescription

        textViewGroupTitle = (TextView) rootView.findViewById(R.id.groupTitle);
        textViewFenceName= (TextView) rootView.findViewById(R.id.groupFenceLocation);
        textViewGroupDesc = (TextView) rootView.findViewById(R.id.groupDescription);

        btnGroupChat = (ImageButton) rootView.findViewById(R.id.btnGroupChat);
        btnGroupTask = (ImageButton) rootView.findViewById(R.id.btnGroupTask);
        fabGroupSetting = (FloatingActionButton) rootView.findViewById(R.id.fabGroupSetting);

        groupMembers = (ListView) rootView.findViewById(R.id.listViewGroupMembers);
    }

    private void setListeners() {
//        btnGroupMembers.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mListener.onGroupHomeFragmentInteraction(OnGroupHomeFragmentInteractionListener.GROUP_MEMBER,null);
//            }
//        });
//
        fabGroupSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onGroupHomeFragmentInteraction(OnGroupHomeFragmentInteractionListener.GROUP_SETTING,null);
            }
        });

        btnGroupChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onGroupHomeFragmentInteraction(OnGroupHomeFragmentInteractionListener.GROUP_CHAT,null);
            }
        });

        btnGroupTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onGroupHomeFragmentInteraction(OnGroupHomeFragmentInteractionListener.GROUP_TASK,groupObj);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupHomeFragmentInteractionListener) {
            mListener = (OnGroupHomeFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnGroupHomeFragmentInteractionListener {
        int GROUP_MEMBER = 1;
        int GROUP_SETTING = 2;
        int GROUP_CHAT = 3;
        int GROUP_TASK = 4;

        void onGroupHomeFragmentInteraction(int constant, @Nullable Object o);
    }
}

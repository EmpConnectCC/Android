package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
<<<<<<< HEAD
import android.support.annotation.Nullable;
=======
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
>>>>>>> origin/master
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.sibtainraza.empconnectcc.Fencing.FencingHandler;
import com.example.sibtainraza.empconnectcc.fencing.FencingHandler;
import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.NavigationDrawerListAdapter;
import com.example.sibtainraza.empconnectcc.database.DbScripts;
import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.utils.AppLog;
import com.example.sibtainraza.empconnectcc.utils.SharedPreferenceManager;

import java.util.ArrayList;

/**

 * Activities that contain this fragment must implement the
 * {@link NavigationDrawerFragment.OnNavigationDrawerFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NavigationDrawerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NavigationDrawerFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ImageView userProfileImage;
    private TextView userName, userEmail;
    private ListView userCheckInGroupsLV;
    private Button checkInOut, logOut;
<<<<<<< HEAD
    private ScrollView navigationDrawerLayout;
=======
    TextView statusTxt;

    private ScrollView navigationDrawerLayout;
    ArrayList<Group> listNavDrawer;
    ArrayList<FencingHandler.FenceInfo> listRegisterFence;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

>>>>>>> origin/master

    private View rootView;
    private OnNavigationDrawerFragmentInteractionListener mListener;

    public NavigationDrawerFragment() {
    }

    public static NavigationDrawerFragment newInstance(String param1, String param2) {
        NavigationDrawerFragment fragment = new NavigationDrawerFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

//        boolean s = SharedPreferenceManager.getManager().getCheckedInStatus();
//        int checkedInFenceId = SharedPreferenceManager.getManager().getCheckedInFenceId();
        pref = getContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();


        initViews();
        setListeners();
        refreshFences();


        boolean s   =   pref.getBoolean("status", false);
        int checkInID = pref.getInt("id", -1);



        if (s == false) {

            statusTxt.setText("Checked out");

        } else if (s == true) {

            statusTxt.setText("Checked IN in fence : "+checkInID);
        }

        return rootView;
    }


    private void initViews() {
        userProfileImage = (ImageView) rootView.findViewById(R.id.navigation_UserProfileImage);
        userName = (TextView) rootView.findViewById(R.id.navigation_TW_UserName);
        userEmail = (TextView) rootView.findViewById(R.id.navigation_TW_UserEmail);
        userCheckInGroupsLV = (ListView) rootView.findViewById(R.id.navigation_ListView);
        logOut = (Button) rootView.findViewById(R.id.navigation_BtnLogout);
        checkInOut = (Button) rootView.findViewById(R.id.navigation_BtnCheckInOut);
<<<<<<< HEAD
=======
        statusTxt = (TextView) rootView.findViewById(R.id.statusTxt);
>>>>>>> origin/master
        navigationDrawerLayout = (ScrollView) rootView.findViewById(R.id.navigationDrawerLayout);
    }

    private void setListeners() {
        userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Profile Image Clicked", Toast.LENGTH_SHORT).show();
                getFragmentManager().beginTransaction().remove(new NavigationDrawerFragment()).commit();
                getFragmentManager().beginTransaction().replace(R.id.frameHomeActivity,new UserProfileFragment()).commit();
//                getFragmentManager().beginTransaction().replace(R.id.frameHomeActivity,new UserProfileFragment()).commit();            }
//                mListener
//                        .OnNavigationDrawerFragmentInteraction(OnNavigationDrawerFragmentInteractionListener.USER_PROFILE,null);

            }
        });

        checkInOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Check InOut Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Logout Clicked", Toast.LENGTH_SHORT).show();
                SharedPreferenceManager.getManager().setUserAuth("");
                mListener.OnNavigationDrawerFragmentInteraction(OnNavigationDrawerFragmentInteractionListener.LOGOUT,null);
            }
        });
    }

<<<<<<< HEAD
    // TODO: Rename method, update argument and hook method into UI event
    //    public void onButtonPressed(Uri uri) {
    //        if (mListener != null) {
    ////            mListener.onFragmentInteraction(uri);
    //        }
    //    }

=======
>>>>>>> origin/master
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNavigationDrawerFragmentInteractionListener) {
            mListener = (OnNavigationDrawerFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNavigationDrawerFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public interface OnNavigationDrawerFragmentInteractionListener {
<<<<<<< HEAD
        int USER_PROFILE = 1;
        void OnNavigationDrawerFragmentInteraction(/*int constant, @Nullable Object o*/);
=======
        int LOGOUT = 1;
        void OnNavigationDrawerFragmentInteraction(int constant, @Nullable Object o);
    }




    void refreshFences() {
        listNavDrawer = new ArrayList<>();

        if (ActivityCompat
                .checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat
                        .checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {   return;
        }


        listNavDrawer= DbScripts.getAllFenceGroups();
        userCheckInGroupsLV.setAdapter(new NavigationDrawerListAdapter(getContext(), listNavDrawer));


        listRegisterFence=DbScripts.getAllFencesToRegister();

        if (listRegisterFence.size() == 0){
            return;
        }

        FencingHandler handler = new FencingHandler(getContext(), listRegisterFence);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean perm = false;
        for (int i = 0; i < permissions.length; i++) {

            String s = permissions[i];
            int j = grantResults[i];
            if (s.equals(Manifest.permission.RECORD_AUDIO)) {

                perm = j == PackageManager.PERMISSION_GRANTED;

            }
        }

        if (requestCode == 0 && perm) {

            Toast.makeText(getContext(), "Permission granted", Toast.LENGTH_SHORT).show();
            refreshFences();

        }

    }

    @Override
    public void onResume() {
        refreshFences();
        super.onResume();
>>>>>>> origin/master
    }
}

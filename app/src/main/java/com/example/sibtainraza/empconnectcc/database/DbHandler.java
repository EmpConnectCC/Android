package com.example.sibtainraza.empconnectcc.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import com.example.sibtainraza.empconnectcc.MyApplication;
import com.example.sibtainraza.empconnectcc.utils.AppLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Muhammad Faizan Khan  on 7/18/2016.
 */
public class DbHandler extends SQLiteOpenHelper {

    private final String TAG = DbHandler.class.getSimpleName();

    protected static String DATABASE_NAME = "database.sqlite";
    protected static String DATABASE_PATH;
    public static final int DATABASE_VERSION = 1;

    private static DbHandler handler;

    /*
        flag for checking initialization
        this happen for api >= 23
        it is due to run time permission

    */
    private static boolean init = false;

    private final Context context;

    public SQLiteDatabase getDatabase() {
        return database;
    }

    private SQLiteDatabase database = null;

    // Constructor
    private DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        DATABASE_PATH = Environment.getExternalStorageDirectory() + "/ConnectCommunication/database/";

        if (!MyApplication.checkPermissionWriteExternal()){
//            Toast.makeText(context,"database:permission not granted",Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        openDataBase();
        init = true;
    }

    public static DbHandler getInstance() {
        return init && handler!=null?handler:(handler = new DbHandler(MyApplication.getInstance()));
    }

    @Override
    public synchronized void close() {
        if (database != null)
            database.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        if (dbExist) {
            AppLog.d(TAG, "database does exist");
        } else {
            AppLog.d(TAG, "database does not exist");
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                e.printStackTrace();
                throw new Error("Error copying database");
            }
        }
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = context.getAssets().open(DATABASE_NAME);
        File file = new File(DATABASE_PATH);
        file.mkdirs();
        String outFileName = DATABASE_PATH + DATABASE_NAME;
        AppLog.d(TAG,"copyDataBase --- "+outFileName);
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private boolean checkDataBase() {

        File dbFile = new File(DATABASE_PATH + DATABASE_NAME);
        AppLog.d(TAG, dbFile + "   "+ dbFile.exists());
        return dbFile.exists();

    }

    public void deleteDataBase() {
        File dbFile = new File(DATABASE_PATH + DATABASE_NAME);
        dbFile.delete();
    }

    boolean openDataBase() throws SQLException {
        String mPath = DATABASE_PATH + DATABASE_NAME;
        AppLog.d(TAG, mPath);
        try {
            database = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.OPEN_READWRITE);
        } catch (Exception e){
            e.printStackTrace();
        }
        //mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        return database != null;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            if (database != null) {
                database.close();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        super.finalize();
    }
}

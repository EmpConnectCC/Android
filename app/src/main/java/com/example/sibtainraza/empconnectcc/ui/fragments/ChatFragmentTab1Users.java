package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.UserAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnChatFragmentUsersInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChatFragmentTab1Users#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragmentTab1Users extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ListView listViewChatUsers;

    private OnChatFragmentUsersInteractionListener mListener;

    public ChatFragmentTab1Users() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ChatFragmentTab1Users newInstance(String param1, String param2) {
        ChatFragmentTab1Users fragment = new ChatFragmentTab1Users();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_chat_tab1_users, container, false);



        initViews();
        setListeners();
        setUpAdapter();


    return  rootView;

    }

    private void setListeners() {

        listViewChatUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TextView name=(TextView)view.findViewById(R.id.user_Name);

                String uname= name.getText().toString();

                Toast.makeText(getContext(),uname,Toast.LENGTH_SHORT).show();

                mListener.onChatUsersFragmentInteraction(OnChatFragmentUsersInteractionListener.CHAT,null);


            }
        });

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChatFragmentUsersInteractionListener) {
            mListener = (OnChatFragmentUsersInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChatFragmentUsersInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void initViews() {

        listViewChatUsers= (ListView)rootView.findViewById(R.id.list_Chat_Users);
    }

    private void setUpAdapter(){
        listViewChatUsers.setAdapter(new UserAdapter(getContext()));

    }

    public static ChatFragmentTab1Users newInstance() {
        ChatFragmentTab1Users fragment = new ChatFragmentTab1Users();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnChatFragmentUsersInteractionListener {


        int CHAT=1;


        // TODO: Update argument type and name
        void onChatUsersFragmentInteraction(int constant, @Nullable Object o);
    }
}

package com.example.sibtainraza.empconnectcc;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.model.User;
import com.example.sibtainraza.empconnectcc.ui.activities.LoginActivity;

public class ConnectCommunicationApplication extends Application {

    private static final String TAG = "ConnectCommunicationApplication";
    private static Context context;
    private static User applicationUser;
    private static Group applicationGroup;
    private static String token;
    public static boolean flag=true;

//    Button btnSignIn = (Button) findViewById(R.id.btn_signIn);

    public static boolean checkConectivity(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static Context getContext() {
        return context;
    }

    public static User getUser() {
        return applicationUser;
    }

    public static void setUser(User user) {
        applicationUser = user;
    }

    public static void setApplicationGroup(Group applicationGroup) {
        ConnectCommunicationApplication.applicationGroup = applicationGroup;
    }

    public static Group getCurrentGroup() {
        return applicationGroup;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this.getApplicationContext();


//        setContentView(R.layout.activity_main);
//        btnSignIn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                startActivity(intent);
//            }
//        });
    }
}

package com.example.sibtainraza.empconnectcc.model;

/**
 * Created by Sibtain Raza on 11/6/2016.
 */
public class User {


    private long userID;
    private String email;
    private String firstName;
    private int profileImg;
    private String designation;
    private String department;
    private String password;

//    private String lastName;
//    private String lastLogin;
//    private String status;
//    private String token; //245 digits firebase access token string, this will also be used as our token,
//    private String userID;
//    private String password;



    // Will be used when user sign up


    public User(){

    }




    public User(String email, String password){
        this.email = email;
        this.password = password;

    }
    public User(
//            String email,
            String firstName,
            int profImg,
            String designation,
            String department){
        this.email = email;
        this.firstName = firstName;
        this.profileImg = profImg;
        this.designation = designation;
        this.department = department;
    }



    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(int profileImg) {
        this.profileImg = profileImg;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }


 /*   public User(String email, String firstName, String lastName, String lastLogin, String status, String token, String userID) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastLogin = lastLogin;
        this.status = status;
        this.token = token;
        this.userID = userID;
    }*/


//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getLastLogin() {
//        return lastLogin;
//    }
//
//    public void setLastLogin(String lastLogin) {
//        this.lastLogin = lastLogin;
//    }
//
//    public String getTaskStatus() {
//        return status;
//    }
//
//    public void setTaskStatus(String status) {
//        this.status = status;
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    public void setToken(String token) {
//        this.token = token;
//    }
//
//    public String getUserID() {
//        return userID;
//    }
//
//    public void setUserID(String userID) {
//        this.userID = userID;
//    }
}

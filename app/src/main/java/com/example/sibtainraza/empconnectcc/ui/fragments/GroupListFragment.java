package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.sibtainraza.empconnectcc.MyApplication;
import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.HomeFragmentCardAdapter;
import com.example.sibtainraza.empconnectcc.database.DbScripts;
import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.utils.AppLog;

import java.util.ArrayList;

/**
 * Created by Sibtain Raza on 11/5/2016.
 */

public class GroupListFragment extends Fragment {

    private static final String TAG = GroupListFragment.class.getSimpleName();

    ImageButton btnAddGroup;
    RecyclerView recyclerView;
    HomeFragmentCardAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Group> groupCard = new ArrayList<Group>();
    private View rootView;

    String name;

    private OnGroupListFragmentInteractionListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_list, container, false);

        initViews();
        setListeners();
        if (ContextCompat.checkSelfPermission(MyApplication.getInstance(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            setUpAdapter();
        } else {
            AppLog.e(TAG,"permission not granted");
        }
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupListFragmentInteractionListener) {
            mListener = (OnGroupListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initViews() {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        btnAddGroup = (ImageButton) rootView.findViewById(R.id.btnAddGroup);
    }

    private void setListeners() {
        btnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onGroupListFragmentInteraction(OnGroupListFragmentInteractionListener.ADD_GROUP, null);
            }
        });
    }

    int count = 0;

    private void setUpAdapter() {

//        int grp_image = R.drawable.logo5;
//        int grp_OwnImage = R.drawable.default_user;
//        String grp_Title, grp_Position;
//        grp_Title = "Group";
//        grp_Position = "Location";

        groupCard = DbScripts.getAllGroups();

//        for (int i = 1; i < 16; i++) {
//            Group group = new Group(grp_Title + " " + i, grp_Position + " " + i, grp_image, grp_OwnImage);
//            count++;
//            groupCard.add(group);
//
//        }

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        adapter = new HomeFragmentCardAdapter(groupCard);
        adapter.setItemSelectedListener(new HomeFragmentCardAdapter.ItemSelectedListener() {
            @Override
            public void onItemSelected(Group group) {
                Toast.makeText(getContext(), "" + group.getGroupTitle(), Toast.LENGTH_SHORT).show();

                mListener.onGroupListFragmentInteraction(OnGroupListFragmentInteractionListener.GROUP_HOME, group);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private boolean validateData() {
        return false;
    }

    public interface OnGroupListFragmentInteractionListener {
        int ADD_GROUP = 1;
        int GROUP_HOME = 2;

        void onGroupListFragmentInteraction(int constant, @Nullable Object o);
    }

} 

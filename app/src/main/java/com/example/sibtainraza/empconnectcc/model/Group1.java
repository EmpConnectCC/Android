package com.example.sibtainraza.empconnectcc.model;

import android.widget.ProgressBar;

/**
 * Created by Sibtain Raza on 11/6/2016.
 */
public class Group1 {
        private String groupTitle;
        private int groupImage;
        private int groupOwnerImage;
        private String groupPhysicalLocation;
        private ProgressBar groupProgressBar;

        public String getGroupTitle() {
            return groupTitle;
        }

        public void setGroupTitle(String groupTitle) {
            this.groupTitle = groupTitle;
        }

        public int getGroupImage() {
            return groupImage;
        }

        public void setGroupImage(int groupImage) {
            this.groupImage = groupImage;
        }

        public int getGroupOwnerImage() {
            return groupOwnerImage;
        }

        public void setGroupOwnerImage(int groupOwnerImage) {
            this.groupOwnerImage = groupOwnerImage;
        }

        public String getGroupPhysicalLocation() {
            return groupPhysicalLocation;
        }

        public void setGroupPhysicalLocation(String groupPhysicalLocation) {
            this.groupPhysicalLocation = groupPhysicalLocation;
        }

        public ProgressBar getGroupProgressBar() {
            return groupProgressBar;
        }

        public void setGroupProgressBar(ProgressBar groupProgressBar) {
            this.groupProgressBar = groupProgressBar;
        }

        public Group1( int groupImage, int groupOwnerImage, String groupTitle,
                      String groupPhysicalLocation, ProgressBar groupProgressBar) {

            this.setGroupImage(groupImage);
            this.setGroupOwnerImage(groupOwnerImage);

            this.setGroupTitle(groupTitle);
            this.setGroupPhysicalLocation(groupPhysicalLocation);
            this.setGroupProgressBar(groupProgressBar);

        }


}

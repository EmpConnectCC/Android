//package com.example.sibtainraza.empconnectcc.adapters;
//
//import android.app.Activity;
//import android.content.Context;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.PopupMenu;
//import android.widget.TextView;
//
//import com.android.volley.toolbox.ImageLoader;
//import com.bumptech.glide.Glide;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//
//import java.util.ArrayList;
//
//import de.hdodenhof.circleimageview.CircleImageView;
//import employeeconnect.panacloud.com.R;
//import employeeconnect.panacloud.com.firebase.FirebaseArrayListAdapter;
//import employeeconnect.panacloud.com.firebase.FirebaseHandler;
//import employeeconnect.panacloud.com.httputils.HttpRequestHandler;
//import employeeconnect.panacloud.com.models.Group;
//import employeeconnect.panacloud.com.models.User;
//import employeeconnect.panacloud.com.services.group.GroupService;
//import employeeconnect.panacloud.com.services.listeners.ServiceError;
//import employeeconnect.panacloud.com.services.listeners.ServiceListener;
//import employeeconnect.panacloud.com.utils.AppLog;
//import employeeconnect.panacloud.com.utils.Util;
//
///**
// * Created by Sibtain on 14/07/2015.
// */
//public class GroupMemberAdapter extends FirebaseArrayListAdapter<GroupMemberAdapter.GroupMemberData> {
//    private int switchValue; // GETTING VALUE BY WHICH IDENTIFYING WHICH LISTVIEW HAS TO BE REQUEST FROM THE FIREBASE
//    public static final int BLOCKED_MEMBERS = 1, INVITATION_REQUEST = 2, ALL_MEMBERS = 3;
//
//    private Context context;
//    private int mResource;
//    private static final String TAG = "GroupTaskAdapter";
//    private Group mGroup;
//    private ArrayList<GroupMemberData> mMemberDataList;
//    private ArrayList<GroupMemberData> mMemberDataListBackup;
//
//    private Button popUpButton;
//    private GroupMemberAdapter groupMemberAdapter;
//    private ServiceListener<GroupMemberData> mAddItemCallBack;
//
//    private int mCurrentFilterLength;
//
//    public GroupMemberAdapter(Activity c, int layoutResourse, Group group, int switchValue, ArrayList<GroupMemberData> list) {
//        super(FirebaseHandler.getInstance().getGroupsRef(), GroupMemberData.class, layoutResourse, c, list);
//
//        AppLog.v(TAG + "subgroup", "After subgroup constructor start after super");
//        AppLog.d("SwitchValueBaseConstructor1", switchValue + " and " + this.switchValue);
//        this.context = c.getBaseContext();
//        this.mResource = layoutResourse;
//        this.mGroup = group;
//        this.switchValue = switchValue;
//        AppLog.d("SwitchValueBaseConstructor2", switchValue + " and " + this.switchValue);
//        firebaseRequest();
//        AppLog.v(TAG + "subgroup", "After subgroup constructor end");
//        this.mMemberDataList = list;
//        this.mMemberDataListBackup = new ArrayList<>(list);
//        AppLog.d("SwitchValueMainAdapterOne", switchValue + "");
//        groupMemberAdapter = this;
//    }
//
//    public GroupMemberAdapter(Activity c, int layoutResourse, Group group, ArrayList<GroupMemberData> list, int switchValue, ServiceListener<GroupMemberData> addItemCallBack) {
//        this(c, layoutResourse, group, switchValue, list);
//        AppLog.d("SwitchValueMainAdapterTwo", switchValue + "");
//        this.mAddItemCallBack = addItemCallBack;
//    }
///*
//
//    private void baseConstructorWork(Activity c, int layoutResourse, Group group, int switchValue) {
//        AppLog.v(TAG + "subgroup", "After subgroup constructor start after super");
//        AppLog.d("SwitchValueBaseConstructor1",switchValue+" and " + this.switchValue);
//        this.context = c.getBaseContext();
//        this.mResource = layoutResourse;
//        this.mGroup = group;
//        this.switchValue = switchValue;
//        AppLog.d("SwitchValueBaseConstructor2",switchValue+" and " + this.switchValue);
//        firebaseRequest();
//        AppLog.v(TAG + "subgroup", "After subgroup constructor end");
//    }
//*/
//
//    // SWITCH FOR SETTING LISTVIEW ACCORDING TO TABS
//    private void firebaseRequest() {
//        switch (switchValue) {
//            case GroupMemberAdapter.ALL_MEMBERS:
//                AppLog.d(TAG + "requestForAllGroupMembers", " request called for all members");
//                GroupService.requestForAllGroupMembers(this);
//                break;
//            case GroupMemberAdapter.INVITATION_REQUEST:
//                AppLog.d(TAG + "requestForAllGroupMembers", " request called for invitation");
//                GroupService.requestForInivitationRequestMembers(this);
//                break;
//            case GroupMemberAdapter.BLOCKED_MEMBERS:
////                todo: firebase request for blocked members
//                AppLog.d(TAG + "requestForAllGroupMembers", " request called for blocked members");
//                GroupService.requestForBlockedMembers(this);
//                break;
//            default:
//                AppLog.d(TAG + "requestForAllGroupMembers", " no request called");
//        }
//    }
//
//    @Override
//    public void addEventListener() {
//    }
//
//    @Override
//    protected void populateView(View convertView, final GroupMemberData model) {
//        final View singleView = convertView;
//
//        final ImageLoader imageLoader = HttpRequestHandler.getInstance().getImageLoader();
//        final CircleImageView memberProfilePicture = (CircleImageView) singleView.findViewById(R.id.userSettingMemberIcon);
//
//        final TextView memberNameTextView = (TextView) singleView.findViewById(R.id.Name);
//        final TextView memberEmailTextView = (TextView) singleView.findViewById(R.id.Email);
//        final TextView memberContactTextView = (TextView) singleView.findViewById(R.id.Contact);
//
//        popUpButton = (Button) singleView.findViewById(R.id.popUpButton);
//
//        ArrayAdapter<CharSequence> popUpAdapter = ArrayAdapter.createFromResource(getContext(),
//                R.array.userSettings_AllMembers, android.R.layout.simple_spinner_item);
//
//        memberNameTextView.setText(model.getMemberName());
//        memberEmailTextView.setText(model.getMemberEmail());
//        memberContactTextView.setText(model.getMemberContact());
////        memberProfilePicture.setImageUrl(model.getImageUrl(), imageLoader);
//
//        if(model.getImageUrl() != null){
//            Glide.with(context).load(model.getImageUrl()).error(R.drawable.default_user).into(memberProfilePicture);
//
////            imageLoader.get(model.getImageUrl(),new ImageLoader.ImageListener(){
////
////                @Override
////                public void onErrorResponse(VolleyError volleyError) {
////
////                }
////
////                @Override
////                public void onResponse(ImageLoader.ImageContainer imageContainer, boolean b) {
////                    memberProfilePicture.setImageBitmap(imageContainer.getBitmap());
////                }
////            });
//        }else Glide.with(context).load(Util.defaultUserImageUrl).error(R.drawable.default_user).into(memberProfilePicture);
//
//        if(switchValue == GroupMemberAdapter.INVITATION_REQUEST){
//            popupInvitationRequest(model);
//        }
//
//        if (getCount() == getPosition(model)) {
//            singleView.setPadding(0, 0, 0, 50);
//            AppLog.d("Padding", "" + singleView.getPaddingBottom());
//        }
//// SWITCH FOR POPUP
//        switch (switchValue) {
//            case GroupMemberAdapter.ALL_MEMBERS:
//                AppLog.d(TAG + "popup", " popup for all members");
//                popupAllMember(model);
//                break;
//            case GroupMemberAdapter.INVITATION_REQUEST:
//                AppLog.d(TAG + "popup", " popup for invitation");
//                popupInvitationRequest(model);
//                break;
//            case GroupMemberAdapter.BLOCKED_MEMBERS:
//                popupBlockMember(model);
//                AppLog.d(TAG + "popup", " popup for blocked members");
//                break;
//            default:
//                AppLog.d(TAG + "popup", " no popup called");
//        }
//    }
//
//    // SWITCH FOR POPUP - popupAllMember
//    // 1) popupAllMember (1st Screen - All Members)
//    private void popupAllMember(final GroupMemberData memberData) {
//        AppLog.d("memberData.getMembershipType()", memberData.getMembershipType() + "");
//        switch (memberData.getMembershipType()) {
//            // -) When the user is Admin.
//            case 2: {
//                popUpButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        PopupMenu popupMenu;
//                        popupMenu = new PopupMenu(getContext(), popUpButton);
//                        popupMenu.getMenuInflater().inflate(R.menu.popup_menu_all_group_members_ifadmin, popupMenu.getMenu());
//
//                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            @Override
//                            public boolean onMenuItemClick(MenuItem item) {
//
//                                switch (item.getItemId()) {
//                                    case R.id.makeUserMember:
//                                        Util.successToast("making member");
//                                        makeUserMemberOfGroup(memberData);
//                                        break;
//
//                                    case R.id.deleteMember:
//                                        Util.successToast("deleting member");
//                                        deleteUserFromGroup(memberData);
//                                        break;
//
//                                    case R.id.makeUseradmin:
//                                        Util.successToast("Already Admin");
//                                        break;
//
//                                    case R.id.blockMember:
//                                        Util.successToast("blocking member");
//                                        blockUserFromGroup(memberData);
//                                        break;
//
//                                    default:
//                                        Util.successToast("You Clicked : " + "default");
//                                }
//
//                                AppLog.d("itemID", item.getItemId() + "");
//
//                                return false;
//                            }
//                        });
//                        popupMenu.show();
//
//                    }
//
//                });
//            }
//            break;
//            // -) When the user is a Member
//            case 3: {
//                popUpButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        PopupMenu popupMenu;
//                        popupMenu = new PopupMenu(getContext(), popUpButton);
//                        popupMenu.getMenuInflater().inflate(R.menu.popup_menu_all_group_members_ifadmin, popupMenu.getMenu());
//
//                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                            @Override
//                            public boolean onMenuItemClick(MenuItem item) {
//
//                                switch (item.getItemId()) {
//                                    case R.id.makeUseradmin:
//                                        Util.successToast("making admin");
//                                        makeUserAdminOfGroup(memberData);
//                                        break;
//
//                                    case R.id.deleteMember:
//                                        Util.successToast("deleting member");
//                                        deleteUserFromGroup(memberData);
//                                        break;
//                                    case R.id.makeUserMember:
//                                        Util.successToast( "Already member");
//                                        break;
//                                    case R.id.blockMember:
//                                        Util.successToast("blocking member");
//                                        blockUserFromGroup(memberData);
//                                        break;
//
//                                    default:
//                                        Util.successToast("You Clicked : " + "default");
//                                }
//                                return false;
//                            }
//                        });
//                        popupMenu.show();
//                    }
//                });
//            }
//            break;
//            // -) When the user is Owner.
//            default:
//                AppLog.d(TAG + "popup", "Invalid MemberShipType");
////                Util.successToast("Invalid MemberShipType");
//                break;
//            case 1:
//                popUpButton.setClickable(false);
//                popUpButton.setBackgroundResource(R.drawable.ic_account_circle_black_24dp);
//                break;
//        }
//    }
//
//
//
//    // 2) popupInvitationRequest (2nd Screen - Invitation/Request Members)
//    private void popupInvitationRequest(final GroupMemberData memberData) {
//        popUpButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PopupMenu popupMenu;
//                popupMenu = new PopupMenu(getContext(), popUpButton);
//                popupMenu.getMenuInflater().inflate(R.menu.popup_menu_invitation_request_group_members, popupMenu.getMenu());
//
//                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        Util.successToast("You Clicked inv-no case: " + item.getTitle());
//                        switch (item.getItemId()) {
//                            case R.id.acceptUser:
//                                accceptUserInvitation(memberData);
//                                break;
//
//                            case R.id.deleteUser:
//                                ignoreUserInvitation(memberData);
//                                break;
//
//                            default:
//                        }
//                        return false;
//                    }
//                });
//                popupMenu.show();
//            }
//
//        });
//
//    }
//
//    // 3) popupBlockMember (3rd Screen - Blocked Members)
//    private void popupBlockMember(final GroupMemberData memberData) {
//        popUpButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PopupMenu popupMenu;
//                popupMenu = new PopupMenu(getContext(), popUpButton);
//                popupMenu.getMenuInflater().inflate(R.menu.popup_menu_blocked_members, popupMenu.getMenu());
//
//                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//
//                        switch (item.getItemId()) {
//
//                            case R.id.deleteUser:
//                                Util.successToast("deleting member");
//                                deleteUserFromGroup(memberData);
//                                break;
//                            case R.id.unBlockMember:
//                                Util.successToast("making member");
//                                makeUserMemberOfGroup(memberData);
//                                break;
//                            default:
//                                Util.successToast("You Clicked : " + "default");
//                        }
//                        return false;
//                    }
//                });
//                popupMenu.show();
//
//            }
//
//        });
//    }
//
//    // changeUserMembershipType (For Changing the MemberShip from Admin to a Member or Vice Versa)
//    public void changeUserMembershipType(String userID, int newMembership) {
//        int i = 0;
////        while (i<mMemberDataList.size() && mMemberDataList.)
//        while (i < getCount() && !getItem(i).compareID(userID)) i++;
//
//        if (i < getCount()) {
//            getItem(i).setMembershipType(newMembership);
//        }
//        notifyDataSetChanged();
//    }
//
//    // removeMember (Call From Firebase When onChildRemoved Trigger, Mention in GroupService, USING FOR REMOVING FROM THE LISTVIEW)
//    public void removeMember(String userID) {
//        int i = 0;
////        while (i<mMemberDataList.size() && mMemberDataList.)
//        while (i < getCount() && !getItem(i).compareID(userID)) i++;
//
//        if (i < getCount()) {
//            remove(getItem(i));
//        }
//        notifyDataSetChanged();
//    }
//
//    // makeUserMemberOfGroup (Call when "Member" option is selected from the popUp)
//    private void makeUserMemberOfGroup(GroupMemberData memberData) {
//
//        GroupService.makeMemberOfGroup(mGroup, memberData,SubGroupAdapter.getInstance().getSubGroupDataList(), new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                notifyDataSetChanged();
//            }
//
//        });
//    }
//
////    makeUserAdminOfAllSubgroups (Call when "Admin" option is selected from the popUp)
//    private void makeUserAdminOfAllSubgroups(GroupMemberData memberData) {
//
//
//    }
//
//    // makeUserAdminOfGroup (Call when "Admin" option is selected from the popUp)
//    private void makeUserAdminOfGroup(final GroupMemberData memberData) {
//
//        GroupService.makeAdminOfGroup(mGroup, memberData,SubGroupAdapter.getInstance().getSubGroupDataList() ,new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
////                makeUserAdminOfAllSubgroups(memberData);
//                notifyDataSetChanged();
//
//            }
//        });
//    }
//
//    // deleteUserFromGroup (Call when "Delete" option is selected from the popUp)
//    private void deleteUserFromGroup(GroupMemberData memberData) {
//        GroupService.performUserDeletionFromGroup(mGroup, memberData, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                notifyDataSetChanged();
//
//            }
//
//        });
//    }
//
//    private void blockUserFromGroup(GroupMemberData memberData) {
//        GroupService.blockUserInGroup(mGroup, memberData, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                notifyDataSetChanged();
//            }
//
//
//        });
//    }
//
//    // deleteUserFromGroup (Call when "Accept" option is selected from the popUp)
//    private void accceptUserInvitation(GroupMemberData memberData) {
//        GroupService.performUserRequestAccept(mGroup, memberData, new ServiceListener<User>() {
//            @Override
//            public void success(User obj) {
//                notifyDataSetChanged();
//            }
//
//            @Override
//            public void error(ServiceError serviceError) {
//
//            }
//        });
//    }
//
//
//    // deleteUserFromGroup (Call when "ignore" option is selected from the popUp)
//    private void ignoreUserInvitation(GroupMemberData memberData) {
//        GroupService.performUserRequestDelete(mGroup, memberData, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                notifyDataSetChanged();
//            }
//
//        });
//    }
//
//    @Override
//    public void add(GroupMemberData object) {
//        super.add(object);
//        mMemberDataListBackup.add(object);
//    }
//
//    public void filterMembers(String s) {
////        when start filtering
//        int filterLength = s.length();
//        if (filterLength == 0 || filterLength < mCurrentFilterLength) {
//            mCurrentFilterLength = filterLength;
//            mMemberDataList.clear();
//            mMemberDataList.addAll(mMemberDataListBackup);
//            if (filterLength == 0){
//                notifyDataSetChanged();
//                return;
//            }
//        }
//
//        int i=0;
//        while (i<mMemberDataList.size()){
//            if (!mMemberDataList.get(i).getMemberName().toLowerCase().contains(s)){
//                mMemberDataList.remove(i);
//            } else {
//                i++;
//            }
//        }
//        mCurrentFilterLength = filterLength;
//        notifyDataSetChanged();
//
//    }
//
//    @Override
//    public GroupMemberData getItem(int position) {
//        return mMemberDataList.get(position);
//    }
//
//    // GroupMemberData CLASS
//    public static class GroupMemberData {
//
//        private String mID;
//        private String mName;
//        private String mLastName;
//        private String mEmail;
//        private String mContact;
//        private String mImageUrl;
//        private int membershipType;
//
//        public GroupMemberData() {
//        }
//
//        public GroupMemberData(String groupMemberID, String groupMemberName, String groupMemberEmail
//                , String groupMemberContact, String groupMemberUrl) {
//            this.mID = groupMemberID;
//            this.mName = groupMemberName;
//            this.mEmail = groupMemberEmail;
//            this.mContact = groupMemberContact;
//            this.mImageUrl = groupMemberUrl;
//        }
//
//        public String getID() {
//            return mID;
//        }
//
//        public void setID(String mID) {
//            this.mID = mID;
//        }
//
//        public String getMemberName() {
//            return mName;
//        }
//
//        public String getMemberEmail() {
//            return mEmail;
//        }
//
//        public String getMemberContact() {
//            return mContact;
//        }
//
//        public String getImageUrl() {
//            return mImageUrl;
//        }
//
//        public int getMembershipType() {
//            return membershipType;
//        }
//
//        public void setMemberName(String groupMemberName) {
//            this.mName = groupMemberName;
//        }
//
//        public void setMemberEmail(String groupMemberEmail) {
//            this.mEmail = groupMemberEmail;
//        }
//
//        public void setMemberContact(String groupMemberContact) {
//            this.mContact = groupMemberContact;
//        }
//
//        public void setMemberImageUrl(String groupMemberUrl) {
//            this.mImageUrl = groupMemberUrl;
//        }
//
//        public void setMembershipType(int membershipType) {
//            this.membershipType = membershipType;
//        }
//
//        public boolean compareID(String id) {
//            return mID.equals(id);
//        }
//
//        public String getLastName() {
//            return mLastName;
//        }
//
//        public void setLastName(String mLastName) {
//            this.mLastName = mLastName;
//        }
//
//        public String getFullName(){
//            return getMemberName()+" "+getLastName();
//        }
//    }
//
//
//}
//

//package com.example.sibtainraza.empconnectcc.utils;
//
//import android.content.Context;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.squareup.okhttp.OkHttpClient;
//import com.unation.app.BuildConfig;
//import com.unation.app.UnationApplication;
//import com.unation.app.exception.HttpServiceNotInitlialized;
//
//import retrofit.RequestInterceptor;
//import retrofit.RestAdapter;
//import retrofit.client.OkClient;
//import retrofit.converter.GsonConverter;
//
///**
// * Created by asultan on 8/19/2015.
// */
//public class HttpUtils {
//
//    private static String BASE_URL;
//    private static Context ctx;
//
//    static {
//            BASE_URL = "http://qa-api.unation.com/";
//    }
//
//    public static final int HTTP_SUCCESS = 200;
//    public static final int HTTP_UNAUTHORIZED = 401;
//
//    static RestAdapter restAdapter;
//    static iWebServices service;
//
//    public static void initializeHttpService() {
//
//        if (restAdapter == null) {
//
//            Gson gson = new GsonBuilder()
//                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//                    .create();
//
//            restAdapter = new RestAdapter.Builder()
//                    .setEndpoint(BASE_URL)
//                    .setRequestInterceptor(new MyRetrofitInterceptor())
//                    .setConverter(new GsonConverter(gson))
//                    .setClient(new OkClient(new OkHttpClient()))
////                    .setLogLevel(RestAdapter.LogLevel.FULL)
////                    .setLog(new AndroidLog("Retrofit Logging"))
//                    .setClient(new OkClient(new OkHttpClient()))
//                            //.setErrorHandler(new HttpErrorHandler())
//                    .build();
//        }
//        if (service == null) {
//            service = restAdapter.create(iWebServices.class);
//        }
//    }
//
//    public static iWebServices getHttpService(Context ctx) {
//        HttpUtils.ctx = ctx;
//        if (service == null || restAdapter == null) {
//            throw new HttpServiceNotInitlialized();
//        }
//        return service;
//    }
//
//    public static boolean isInternetConnected(Context ctx) {
//        ConnectivityManager connectivityManager
//                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }
//
//    public static class MyRetrofitInterceptor implements RequestInterceptor {
//
//        @Override
//        public void intercept(RequestFacade req) {
//
//            if (SessionManager.getSessionManager(UnationApplication.getInstance().getActivity()).isLoggedIn()) {
//                 req.addHeader("token", SessionManager.getSessionManager(ctx).getUserDetails().get(SessionManager.KEY_TOKEN));
//            }
//            req.addHeader("client_id", "4a968212");
//            req.addHeader("client_secret", "da402a3935cb5ef21159ce8893b23c5d");
//            //req.addHeader("token", "7eUokO8Wy0OvIadrigNB+MpLsOAqQE4TbQcsJSISblknbltLn0xksBXO5keT50wWk0352Xtx6Zg=");
//
//            //unationtester1@gmail
//            //req.addHeader("token", "2gpb2tJ2xJnJyL1vAMcByWNKeJVJwy5WVXUgFAVFPKhkBWJ2huLO1Mx3r5+f9G00usWcXmlxV5I=");//req.addHeader("token", "+LNgVlJ4Nc61cibqZHXGTi5zA+QcDgmIqGPrasKlmKcvazjQx2IA8SEO0beoNVv/MVHS7OVMVh4=");
//
//            //kraza@westagilelabs
//            //req.addHeader("token", "m47l7upSO8TJyL1vAMcByVJSUeU8kEY2lw0Ri7e91XRkBWJ2huLO1NlZSfXnPuuZusWcXmlxV5I=");
//
//            //smkrn_89@hotmail
//            //req.addHeader("token", "m47l7upSO8RQtqDHpXuYmNNI0OE3qqYJzbcdJSLPdQqSwb9hLImpubwoP/DZQF6kk0352Xtx6Zg=");
//        }
//    }
//
//}

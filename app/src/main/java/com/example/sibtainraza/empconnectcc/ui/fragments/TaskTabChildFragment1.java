package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.TaskAdapter;
import com.example.sibtainraza.empconnectcc.database.DbScripts;
import com.example.sibtainraza.empconnectcc.model.Task;
import com.example.sibtainraza.empconnectcc.utils.AppLog;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnTaskTabChildFragment1InteractionListener} interface
 * to handle interaction events.
 * Use the {@link TaskTabChildFragment1#newInstance} factory method to
 * create an instance of this fragment.
 */

public class TaskTabChildFragment1 extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private long groupID;

    EditText groupTaskTab1EditText;
    Spinner groupTaskTab1Spinner;
    ListView groupTaskTab1listView;
    ArrayList<Task> listViewAssignedTask = new ArrayList<Task>();
//    TaskAdapter taskAdapter;
    FloatingActionButton groupTaskAddButton;
    ArrayAdapter<CharSequence> SpinnerAdapter;
//    private GroupAddTaskFragment.OnTaskTabChildFragment1InteractionListener mListener;
    ArrayAdapter<CharSequence> popUpAdapter;

    EditText edTaskName;
    EditText edTaskDesc;
    Button btnAddTask;
    Button btnCancelTask;

    private OnTaskTabChildFragment1InteractionListener mListener;

    public TaskTabChildFragment1() {
        // Required empty public constructor
    }

    public static TaskTabChildFragment1 newInstance(long Id) {
        TaskTabChildFragment1 fragment = new TaskTabChildFragment1();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1,Id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groupID = getArguments().getLong(ARG_PARAM1);
        AppLog.d("GroupObject - ",groupID+"");

        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.task_tab_child_fargment_1, container, false);

        groupTaskTab1EditText = (EditText) rootView.findViewById(R.id.groupTaskTab1EditText);
//        groupTaskTab1Spinner = (Spinner) rootView.findViewById(R.id.groupTaskTab1Spinner);
        groupTaskTab1listView = (ListView) rootView.findViewById(R.id.listAllTasks);
        groupTaskAddButton  = (FloatingActionButton) rootView.findViewById(R.id.groupTaskAddButton);

        SpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.screen_Name, android.R.layout.simple_spinner_item);

        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        groupTaskTab1Spinner.setAdapter(SpinnerAdapter);

        listViewAssignedTask = DbScripts.getAllAssignedTask(groupID);
        AppLog.v("TASKS", listViewAssignedTask +"");
//        taskAdapter = new TaskAdapter(getContext(), listViewAssignedTask);
        groupTaskTab1listView.setAdapter(new TaskAdapter(getContext(), listViewAssignedTask));
//        groupTaskAdapter = new groupTaskAdapter(getActivity(),R.layout.single_item_list_view1,
//                EmployeeConnectApplication.getCurrentGroup(),GroupTaskAdapter.INVITATION_REQUEST,
//                new ArrayList<GroupTaskAdapter.GroupTaskData>());


        groupTaskAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Need To Be Implemented",Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

                LayoutInflater inflaterDialog=getActivity().getLayoutInflater();
                View rootViewDialogBox=inflaterDialog.inflate(R.layout.add_task_dialog_fragment,null);

                edTaskName= (EditText) rootViewDialogBox.findViewById(R.id.taskNameDialog);
                edTaskDesc= (EditText) rootViewDialogBox.findViewById(R.id.taskDescDialog);
                btnAddTask = (Button) rootViewDialogBox.findViewById(R.id.btnAddTaskDialog);
                btnCancelTask = (Button) rootViewDialogBox.findViewById(R.id.btnCancelTaskDialog);

                builder.setView(rootViewDialogBox);

                final AlertDialog dialog = builder.create();

                btnAddTask.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String taskName = edTaskName.getText().toString().trim();
                        String taskDescription = edTaskDesc.getText().toString().trim();
                        long taskId = DbScripts.createTask(groupID,taskName,taskDescription,1,"30-12-16");
                        Toast.makeText(getContext(), "Task Added",Toast.LENGTH_SHORT).show();
                    }
                });

                btnCancelTask.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getContext(), "Cancel",Toast.LENGTH_SHORT).show();
                        dialog.cancel();
//                      dialog.hide();
                    }
                });
                dialog.show();

            }
        });

//                mListener.OnTaskTabChildFragment1InteractionListener(OnTaskTabChildFragment1InteractionListener.ADD_TASK,null);
//                TaskTabChildFragment1 fragment = new TaskTabChildFragment1();
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
////        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
//                fragmentTransaction.replace(R.id.frameHomeActivity, fragment);
//                fragmentTransaction.commit();
//        public interface OnTaskTabChildFargment1Interaction {
//
//            int GROUP_MEMBER = 1;
//            int GROUP_SETTING = 2;
//            int GROUP_CHAT = 3;
//            int GROUP_TASK = 4;
//
//            void OnTaskTabChildFragment1InteractionListener(int constant, @Nullable Object o);
//        }

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTaskTabChildFragment1InteractionListener) {
            mListener = (OnTaskTabChildFragment1InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTaskTabChildFragment1InteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnTaskTabChildFragment1InteractionListener {
        // TODO: Update argument type and name
        int ADD_TASK = 1;
        void OnTaskTabChildFragment1InteractionListener(int constant, @Nullable Object o);
    }
}

package com.example.sibtainraza.empconnectcc.model;

/**
 * Created by MuhammadAli on 20-Oct-16.
 */
public class SingleFence {

   private long Id;
    private long Radius;
    private double latitude;
    private double longitude;

    public SingleFence(){

    }



    public SingleFence(long id, long radius, double latitude, double longitude){
        this.Id=id;
       this.Radius=radius;
        this.latitude=latitude;
        this.longitude=longitude;

    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public long getRadius() {
        return Radius;
    }

    public void setRadius(long radius) {
        Radius = radius;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}

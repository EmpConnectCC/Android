package com.example.sibtainraza.empconnectcc.model;

/**
 * Created by Sibtain Raza on 11/6/2016.
 */
public class User1 {
    private String email;
    private String firstName;
    private String lastName;
    private String lastLogin;
    private String status;
    private String token; //245 digits firebase access token string, this will also be used as our token,
    private String userID;
    private String password;
    private String profileImg;


    public User1() {
    }

    // Will be used when user sign up
    public User1(String email, String userID, String firstName, String lastName,String password) {
        this.email = email;
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.profileImg = profileImg;
    }

    public User1(String email, String firstName, String lastName, String lastLogin, String status, String token, String userID) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastLogin = lastLogin;
        this.status = status;
        this.token = token;
        this.userID = userID;
    }

//sibtain

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getProfileImg() {
        return profileImg!=null?profileImg:"https://s3-us-west-2.amazonaws.com/defaultimgs/user.png";
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }
}

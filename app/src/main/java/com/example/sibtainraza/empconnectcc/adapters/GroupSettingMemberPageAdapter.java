package com.example.sibtainraza.empconnectcc.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment1;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment2;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment3;

public class GroupSettingMemberPageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public GroupSettingMemberPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0: // Groups OWNER ( Make Admin -> Member || Make Admin -> Member )
                TaskTabChildFragment1 tab1 = new TaskTabChildFragment1();
                return tab1;
            case 1: // Add Groups Member
                TaskTabChildFragment2 tab2 = new TaskTabChildFragment2();
                return tab2;
            case 2: // Remove Groups Member
                TaskTabChildFragment3 tab3 = new TaskTabChildFragment3();
                return tab3;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}




package com.example.sibtainraza.empconnectcc.model;

/**
 * Created by Sibtain Raza on 11/13/2016.
 */
public class GroupMember {

    private int groupMemberImage;
    private String groupMemberName;

    public GroupMember(int groupMemberImage, String groupMemberName) {
        this.groupMemberImage = groupMemberImage;
        this.groupMemberName = groupMemberName;}


    public int getGroupMemberImage() {
        return groupMemberImage;
    }

    public void setGroupMemberImage(int groupMemberImage) {
        this.groupMemberImage = groupMemberImage;
    }

    public String getGroupMemberName() {
        return groupMemberName;
    }

    public void setGroupMemberName(String groupMemberName) {
        this.groupMemberName = groupMemberName;
    }

}

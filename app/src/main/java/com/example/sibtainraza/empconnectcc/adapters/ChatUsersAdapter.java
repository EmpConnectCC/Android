package com.example.sibtainraza.empconnectcc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.model.User;

import java.util.ArrayList;

/**
 * Created by MuhammadAli on 18-Nov-16.
 */
public class ChatUsersAdapter extends BaseAdapter {

    ArrayList<User> listViewChatUser;
    Context context;

    public ChatUsersAdapter(Context context) {
        this.context=context;
        listViewChatUser =new ArrayList<User>();
        ///Data  Filling in listViewChat object
//        Resources res=context.getResources();
        String email="khan@live.com";
        String name="User";
        String designation="Designation";
        String department="Department";
        int img= R.drawable.listview_img_user;

        for (int i=1;i<15;i++){
            listViewChatUser.add(new User(name+" "+i,img,designation+" "+i,department+" "+i));
        }
    }

    @Override
    public int getCount() {
        return listViewChatUser.size();
    }

    @Override
    public Object getItem(int i) {
        return listViewChatUser.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rootView= inflater.inflate(R.layout.single_item_lv_employee,viewGroup,false);


        TextView name=(TextView)rootView.findViewById(R.id.user_Name);
        TextView department=(TextView)rootView.findViewById(R.id.user_Department);
        TextView designation=(TextView)rootView.findViewById(R.id.user_desigation);

        ImageView img= (ImageView)rootView.findViewById(R.id.user_Image);

        User temp= listViewChatUser.get(i);


        name.setText(temp.getFirstName());
        department.setText(temp.getDepartment());
        designation.setText(temp.getDesignation());
        img.setImageResource(temp.getProfileImg());


        return rootView;
    }

}

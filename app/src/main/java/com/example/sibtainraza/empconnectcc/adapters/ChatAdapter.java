package com.example.sibtainraza.empconnectcc.adapters;

/**
 * Created by MuhammadAli on 08-Nov-16.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.model.Chat;

import java.util.ArrayList;

/**
 * Created by MuhammadAli on 08-Nov-16.
 */
public class ChatAdapter extends BaseAdapter {

    ArrayList<Chat> listViewChat;
    Context context;

    public ChatAdapter(Context context) {



        this.context=context;
        listViewChat =new ArrayList<Chat>();



        ///Data  Filling in listViewChat object

//        Resources res=context.getResources();

        String chat_name="chat";
        String chat_msg="message";
        int chat_img= R.drawable.listview_img_user;

        for (int i=1;i<15;i++){
            listViewChat.add(new Chat(chat_name+" "+i,chat_msg,chat_img));
        }
    }

    @Override
    public int getCount() {
        return listViewChat.size();
    }

    @Override
    public Object getItem(int i) {
        return listViewChat.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View row= inflater.inflate(R.layout.single_item_lv_chat,viewGroup,false);

        TextView chatName=(TextView)row.findViewById(R.id.chat_Name);
        TextView chatMsg=(TextView)row.findViewById(R.id.chat_Message);
        ImageView chatImg= (ImageView)row.findViewById(R.id.chat_Image);

        Chat temp= listViewChat.get(i);


        chatName.setText(temp.getChatName());
        chatMsg.setText(temp.getChatMessage());
        chatImg.setImageResource(temp.getChatImg());


        return row;
    }
}

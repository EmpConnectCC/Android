package com.example.sibtainraza.empconnectcc.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.sibtainraza.empconnectcc.fencing.FencingHandler;
import com.example.sibtainraza.empconnectcc.MyApplication;
import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.model.SingleFence;
import com.example.sibtainraza.empconnectcc.model.Task;
import com.example.sibtainraza.empconnectcc.model.User;
import com.example.sibtainraza.empconnectcc.utils.AppLog;

import java.util.ArrayList;

/**
 * Created by Muhammad Faizan Khan on 7/19/2016.
 */
public class DbScripts {
    private static final String TAG = DbScripts.class.getSimpleName();
    private static SQLiteDatabase database;

    static {
        if (MyApplication.checkPermissionWriteExternal()) {
            getDatabase();
        }
    }

    private static void getDatabase() {
        DbHandler dbHandler = DbHandler.getInstance();
        dbHandler.openDataBase();
        database = dbHandler.getDatabase();
        AppLog.i(TAG,"getDatabase --- database = " + database);
    }




/*
  ------------------------------------------------------------------------------------
  Operations For Users
  ------------------------------------------------------------------------------------
*/


    public static ArrayList<User> getAllUsers() {
        String query = "SELECT * FROM " + FeedReaderContract.Users.TABLE_NAME;
        ArrayList<User> userList = new ArrayList<>();

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        User user= null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                user = new User();
//
                user.setUserID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Users.USER_ID)));
                user.setFirstName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Users.USER_FIRST_NAME)));
                user.setProfileImg(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Users.USER_IMAGE)));

                userList.add(user);

            } while (cursor.moveToNext());
            AppLog.v("UserList",userList+"");
        }
//        AppLog.i(TAG, "getGroup --- " + group);

        return userList;
    }

/*
  ------------------------------------------------------------------------------------
  Operations For Groups
  ------------------------------------------------------------------------------------
*/

    public static long createGroup(String groupTitle, int groupImage, String groupDesc,
                                   long groupOwnerID, long groupFenceID, String groupFenceName) {

        ContentValues values = new ContentValues();


        values.put(FeedReaderContract.Groups.GROUP_TITLE, groupTitle);
        values.put(FeedReaderContract.Groups.GROUP_IMAGE, groupImage);
        values.put(FeedReaderContract.Groups.GROUP_DESCRIPTION, groupDesc);
        values.put(FeedReaderContract.Groups.GROUP_OWNER_ID, groupOwnerID);
        values.put(FeedReaderContract.Groups.GROUP_FENCE_ID, groupFenceID);
        values.put(FeedReaderContract.Groups.GROUP_FENCE_NAME, groupFenceName);

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        long group_id;
        synchronized (database) {
            group_id = database.insert(FeedReaderContract.Groups.TABLE_NAME, null, values);
        }

        return group_id;
    }

    public static int updateGroup(long groupId, String groupTitle, String groupImage) {
        ContentValues values = new ContentValues();

        values.put(FeedReaderContract.Groups.GROUP_TITLE, groupTitle);
        values.put(FeedReaderContract.Groups.GROUP_IMAGE, groupImage);

        if (database == null || !database.isOpen()) {
            getDatabase();
        }

        String where = FeedReaderContract.Groups.GROUP_ID + " = " + groupId;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }

        int rowsCount;
        synchronized (database) {
            rowsCount = database.update(FeedReaderContract.Groups.TABLE_NAME, values, where, null);
        }

        //non zero if successful
        //number of rows effected
        return rowsCount;
    }

    public static Group getGroup(long groupId) {

        String query = "SELECT * FROM " +
                FeedReaderContract.Groups.TABLE_NAME +
                " WHERE " + FeedReaderContract.Groups.GROUP_ID + " = " + groupId;

        Cursor cursor = null;
        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        Group group = null;

        if (cursor != null && cursor.moveToFirst()) {

            group = new Group();
//
            group.setGroupID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_ID)));
            group.setGroupTitle(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_TITLE)));
            group.setGroupImage(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_IMAGE)));
            group.setGroupDesc(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_DESCRIPTION)));
            group.setGroupOwnerID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_OWNER_ID)));
            group.setGroupFenceID(cursor.getLong(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_FENCE_ID)));
            group.setGroupFenceName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_FENCE_NAME)));
//            AppLog.i(TAG, "getGroupId --- " + group.getGroupId());
        }
        AppLog.i(TAG, "getGroup --- " + group);
        if (cursor != null) {
            cursor.close();
        }
        return group;
    }

    public static ArrayList<Group> getAllGroups() {
        ArrayList<Group> groupList = new ArrayList<>();
        String query = "SELECT * FROM " + FeedReaderContract.Groups.TABLE_NAME;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        Group group = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                group = new Group();
//
                group.setGroupID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_ID)));
                group.setGroupTitle(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_TITLE)));
                group.setGroupImage(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_IMAGE)));
                group.setGroupDesc(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_DESCRIPTION)));
                group.setGroupOwnerID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_OWNER_ID)));
                group.setGroupFenceID(cursor.getLong(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_FENCE_ID)));
                group.setGroupFenceName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_FENCE_NAME)));

                groupList.add(group);

//            AppLog.i(TAG, "getGroupId --- " + group.getGroupId());
            } while (cursor.moveToNext());
        }


//        AppLog.i(TAG, "getGroup --- " + group);

        return groupList;
    }

    public static ArrayList<Group> getAllFenceGroups() {
        ArrayList<Group> groupList = new ArrayList<>();
        String query = "SELECT group_id,group_title,group_fence_id,group_fence_name FROM " + FeedReaderContract.Groups.TABLE_NAME +
                " WHERE "+FeedReaderContract.Groups.GROUP_FENCE_ID +"<>-1";

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        Group group = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                group = new Group();
//
                group.setGroupID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_ID)));
                group.setGroupTitle(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_TITLE)));
                group.setGroupFenceID(cursor.getLong(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_FENCE_ID)));
                group.setGroupFenceName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_FENCE_NAME)));

                groupList.add(group);

//            AppLog.i(TAG, "getGroupId --- " + group.getGroupId());
            } while (cursor.moveToNext());
        }


//        AppLog.i(TAG, "getGroup --- " + group);

        return groupList;
    }




/*
  ------------------------------------------------------------------------------------
  Operations For Fences
  ------------------------------------------------------------------------------------
*/

    public static ArrayList<FencingHandler.FenceInfo> getAllFencesToRegister() {
        String query = "SELECT fence_id,fence_radius,fence_latitude,fence_longitude FROM " + FeedReaderContract.Fences.TABLE_NAME;
        ArrayList<FencingHandler.FenceInfo> fenceList= new ArrayList<>();


        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        SingleFence fence= null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                FencingHandler.FenceInfo fenceInfo = new FencingHandler.FenceInfo();
//
                fenceInfo.setId(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Fences.FENCE_ID)));
                fenceInfo.setRadius(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Fences.FENCE_RADIUS)));
                fenceInfo.setLatitude(cursor.getDouble(cursor.getColumnIndex(FeedReaderContract.Fences.FENCE_LATITUDE)));
                fenceInfo.setLongitude(cursor.getDouble(cursor.getColumnIndex(FeedReaderContract.Fences.FENCE_LONGITUDE)));

                fenceList.add(fenceInfo);

            } while (cursor.moveToNext());
            AppLog.v("UserList",fenceList+"");
        }
//        AppLog.i(TAG, "getGroup --- " + group);

        return fenceList;
    }



    public static long addFence(String fenceName, int radius, double latitude, double longitude,long fenceIdServer) {

        ContentValues values = new ContentValues();


        values.put(FeedReaderContract.Fences.FENCE_NAME, fenceName);
        values.put(FeedReaderContract.Fences.FENCE_LATITUDE, latitude);
        values.put(FeedReaderContract.Fences.FENCE_LONGITUDE, longitude);
        values.put(FeedReaderContract.Fences.FENCE_RADIUS, radius);
        values.put(FeedReaderContract.Fences.FENCE_ID_SERVER,fenceIdServer);



        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        long fence_id;
        synchronized (database) {
            fence_id = database.insert(FeedReaderContract.Fences.TABLE_NAME, null, values);
        }

        AppLog.d(TAG,"insertData -- id = " + fence_id);
        return fence_id;

    }



/*
  ------------------------------------------------------------------------------------
  Operations For Tasks
  ------------------------------------------------------------------------------------
*/

    public static long createTask(long groupID, String taskName,
                                  String taskDescription, int taskStatus, String taskAssignDate) {

        ContentValues values = new ContentValues();

        values.put(FeedReaderContract.Tasks.GROUP_ID, groupID);
        values.put(FeedReaderContract.Tasks.TASK_NAME, taskName);
        values.put(FeedReaderContract.Tasks.TASK_DESCRIPTION, taskDescription);
        values.put(FeedReaderContract.Tasks.TASK_STATUS, taskStatus);
        values.put(FeedReaderContract.Tasks.TASK_ASSIGN_DATE, taskAssignDate);

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        long taskId;
        synchronized (database) {
            taskId = database.insert(FeedReaderContract.Tasks.TABLE_NAME, null, values);
        }

        return taskId;
    }

    //  Operations For Tasks - Status 1 - ASSIGNED TASKS
    public static ArrayList<Task> getAllAssignedTask(long groupID) {
        ArrayList<Task> taskList = new ArrayList<>();
        String query = "SELECT * FROM " + FeedReaderContract.Tasks.TABLE_NAME
                + " WHERE " + FeedReaderContract.Tasks.GROUP_ID + "=" + groupID + " AND " +FeedReaderContract.Tasks.TASK_STATUS + "=" + 1;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        Task task = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                task = new Task();
//
                task.setTaskID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_ID)));
                task.setGroupID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.GROUP_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_NAME)));
                task.setTaskDescription(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_DESCRIPTION)));
                task.setTaskStatus(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_STATUS)));
                task.setTaskAssignDate(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_ASSIGN_DATE)));

                taskList.add(task);
                AppLog.v("TASKLIST",taskList+"");
            } while (cursor.moveToNext());
        }
//        AppLog.i(TAG, "getGroup --- " + group);

        return taskList;
    }

    //  Operations For Tasks - Status 2 - PROGRESS TASKS
    public static ArrayList<Task> getAllInProgressTask(long groupID) {
        ArrayList<Task> taskList = new ArrayList<>();
        String query = "SELECT * FROM " + FeedReaderContract.Tasks.TABLE_NAME
                + " WHERE " + FeedReaderContract.Tasks.GROUP_ID + "=" + groupID + " AND " +FeedReaderContract.Tasks.TASK_STATUS + "=" + 2;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        Task task = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                task = new Task();
//
                task.setTaskID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_ID)));
                task.setGroupID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.GROUP_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_NAME)));
                task.setTaskDescription(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_DESCRIPTION)));
                task.setTaskStatus(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_STATUS)));
                task.setTaskAssignDate(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_ASSIGN_DATE)));

                taskList.add(task);
                AppLog.v("TASKLIST",taskList+"");
            } while (cursor.moveToNext());
        }
//        AppLog.i(TAG, "getGroup --- " + group);

        return taskList;
    }

    //  Operations For Tasks - Status 3 - COMPLETED TASKS
    public static ArrayList<Task> getAllCompletedTask(long groupID) {
        ArrayList<Task> taskList = new ArrayList<>();
        String query = "SELECT * FROM " + FeedReaderContract.Tasks.TABLE_NAME
                + " WHERE " + FeedReaderContract.Tasks.GROUP_ID + "=" + groupID + " AND " + FeedReaderContract.Tasks.TASK_STATUS + "=" + 3;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        Task task = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                task = new Task();
//
                task.setTaskID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_ID)));
                task.setGroupID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.GROUP_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_NAME)));
                task.setTaskDescription(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_DESCRIPTION)));
                task.setTaskStatus(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_STATUS)));
                task.setTaskAssignDate(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Tasks.TASK_ASSIGN_DATE)));

                taskList.add(task);
                AppLog.v("TASKLIST",taskList+"");
            } while (cursor.moveToNext());
        }
//        AppLog.i(TAG, "getGroup --- " + group);

        return taskList;
    }

}

package com.example.sibtainraza.empconnectcc;


import com.example.sibtainraza.empconnectcc.model.User;
import com.example.sibtainraza.empconnectcc.model.response.UserResp;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IMyApi {
    @POST("user/user")
    Call<UserResp>loginUser(@Body User user);

//    @GET("user?")
//    void paramEx(@Query User user);
//
//    @GET("user/")
//    void pathExample(@Path User user);

}

package com.example.sibtainraza.empconnectcc.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.TaskAdapter;
import com.example.sibtainraza.empconnectcc.database.DbScripts;
import com.example.sibtainraza.empconnectcc.model.Task;
import com.example.sibtainraza.empconnectcc.utils.AppLog;

import java.util.ArrayList;

public class TaskTabChildFragment3 extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private long groupID;

    EditText groupTaskTab3EditText;
    Spinner groupTaskTab3Spinner;
    ArrayAdapter<CharSequence> SpinnerAdapter;
    ListView groupTaskTab3listView;
    ArrayList<Task> listViewCompletedTask = new ArrayList<Task>();
//    GroupTaskAdapter groupMemberAdapter;

    public TaskTabChildFragment3() {
        // Required empty public constructor
    }

    public static TaskTabChildFragment3 newInstance(long Id) {
        TaskTabChildFragment3 fragment = new TaskTabChildFragment3();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1,Id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groupID = getArguments().getLong(ARG_PARAM1);
        AppLog.d("GroupObject - ",groupID+"");
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.task_tab_child_fragment_3, container, false);

//        groupTaskTab3Spinner = (Spinner) rootView.findViewById(R.id.groupTaskTab3Spinner);
        groupTaskTab3listView = (ListView) rootView.findViewById(R.id.listInCompletedTasks);

        SpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.screen_Name, android.R.layout.simple_spinner_item);
        listViewCompletedTask = DbScripts.getAllCompletedTask(groupID);
        groupTaskTab3listView.setAdapter(new TaskAdapter(getContext(),listViewCompletedTask));
//        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        groupTaskTab3Spinner.setAdapter(SpinnerAdapter);
//        listView.setAdapter(new TaskAdapter(getContext()));

//        groupMemberAdapter = new GroupTaskAdapter(getActivity(),R.layout.single_item_list_view1,
//                EmployeeConnectApplication.getCurrentGroup(),GroupTaskAdapter.BLOCKED_MEMBERS,
//                new ArrayList<GroupTaskAdapter.GroupMemberData>());
//
//        listView.setAdapter(groupMemberAdapter);


        return rootView;
    }

}

package com.example.sibtainraza.empconnectcc.ui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sibtainraza.empconnectcc.MyApplication;
import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.database.DbScripts;
import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.model.SignIn;
import com.example.sibtainraza.empconnectcc.model.response.UserResp;
import com.example.sibtainraza.empconnectcc.network.NetworkHandler;
import com.example.sibtainraza.empconnectcc.utils.AppLog;
import com.example.sibtainraza.empconnectcc.utils.SharedPreferenceManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sibtain Raza on 10/17/2016.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private Button signinButton;
    private EditText emailIdEditText;
    private EditText passwordEditText;

    private String email;
    private String password;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        gotoHomeActivity();
//        checkAccessToken();
//        initViews();
//        setUpListeners();
    }

    private void initViews() {
        signinButton = (Button) findViewById(R.id.signin);
        emailIdEditText = (EditText) findViewById(R.id.editTextEmail);
        passwordEditText = (EditText) findViewById(R.id.editTextPassword);

        emailIdEditText.setText("a");
        passwordEditText.setText("a");
    }

    private void setUpListeners() {
        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppLog.i(TAG, "onCreate --- signin - onClick");

                if (!validateData()) {
//                            Util.failureToast("Please enter fields");
                    Toast.makeText(LoginActivity.this, "Please enter fields", Toast.LENGTH_SHORT).show();
                    AppLog.i(TAG, "onCreate --- signin - onClick");
//                            return;
                } else {
                    networkCall();
                }
            }
        });

    }

    private boolean validateData(){
        email = emailIdEditText.getText().toString().trim();
        password = passwordEditText.getText().toString().trim();

        if (email.length() == 0){
            return false;
        }
        if (password.length() == 0){
            return false;
        }

        return true;
    }

    private void gotoHomeActivity() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkAccessToken() {
        String accessToken = SharedPreferenceManager.getManager().getUserAuth();
        if (accessToken != null && accessToken.length() != 0) {
            gotoHomeActivity();
        }
    }

    private void networkCall() {
        if (NetworkHandler.isOnline()) {
            SignIn signIn = new SignIn();

            AppLog.i(TAG, "onCreate --- signin - onClick -- email = " + email +
                    " --- password = " + password);

            signIn.setEmail(email);
            signIn.setPassword(password);

            Call<UserResp> userResp = NetworkHandler.getInstance().getServices().loginUser(signIn);
            userResp.enqueue(new Callback<UserResp>() {
                @Override
                public void onResponse(Call<UserResp> call, Response<UserResp> response) {
                    Toast.makeText(LoginActivity.this, "Response", Toast.LENGTH_SHORT).show();
                    UserResp userResp = response.body();
//                                Toast.makeText(LoginActivity.this, "After Object", Toast.LENGTH_SHORT).show();
                    if (userResp.isValidate()) {
                        Toast.makeText(LoginActivity.this, "Login -> Home", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        SharedPreferenceManager.getManager().setUserAuth(userResp.getAccessToken());

                        Toast.makeText(LoginActivity.this, "AccessToken:" + userResp.getAccessToken(), Toast.LENGTH_SHORT).show();

                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(LoginActivity.this, "Incorrect User", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserResp> call, Throwable t) {

                }
            });
        } else {
                Toast.makeText(LoginActivity.this, "Connectivity Error", Toast.LENGTH_SHORT).show();
        }
    }

//    public static void test() {
//
//        new AsyncTask<Void, Void, Group>() {
//            @Override
//            protected Group doInBackground(Void... voids) {
//                Group group = DbScripts.createGroup();
//                DbScripts.getGroup(1);
//                return group;
//            }
//
//            @Override
//            protected void onPostExecute(Group group) {
//                super.onPostExecute(group);
//            }
//        }.execute();
//    }

}
package com.example.sibtainraza.empconnectcc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.model.GroupMember;

import java.util.ArrayList;

/**
 * Created by Sibtain Raza on 11/12/2016.
 */
public class GroupMemberAdapter extends BaseAdapter{

    ArrayList<GroupMember> groupMemberList;
    Context context;

    public GroupMemberAdapter(Context context){
        this.context = context;
        groupMemberList = new ArrayList<GroupMember>();

        int groupMemberImage = R.drawable.default_user;
        String groupMemberName = "Group Member";


        for (int i=1;i<15;i++){
            groupMemberList.add(new GroupMember(groupMemberImage,groupMemberName+" "+i));
        }
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rootView= inflater.inflate(R.layout.single_item_lv_group_members,viewGroup,false);


        ImageView groupMemberImage=(ImageView) rootView.findViewById(R.id.imageViewGroupMemberImage);
        TextView groupMemberName=(TextView)rootView.findViewById(R.id.textViewGroupMemberName);

        GroupMember groupMember= groupMemberList.get(i);

        groupMemberImage.setImageResource(groupMember.getGroupMemberImage());
        groupMemberName.setText(groupMember.getGroupMemberName());

        return rootView;
    }

    @Override
    public int getCount() {
        return groupMemberList.size();
    }

    @Override
    public Object getItem(int i) {
        return groupMemberList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


}

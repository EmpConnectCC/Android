package com.example.sibtainraza.empconnectcc.database;

/**
 * Created by Sibtain Raza on 12/25/2016.
 */
public class FeedReaderContract {

    public static interface  Users {
        String TABLE_NAME = "Users";

        String USER_ID = "user_id";
        String USER_FIRST_NAME = "user_first_name";
        String USER_EMAIL_ID = "email_id";
        String USER_IMAGE = "user_img";
        String USER_CHECKED_IN_FENCE_ID  = "user_checked_in_fence_id";
        String USER_CHECKED_IN_STATUS = "user_checked_in_status";
    }

    public static interface UserProfile {
        String TABLE_NAME = "Users";
    }

    public static interface Groups {
        String TABLE_NAME = "Groups";

        String GROUP_ID = "group_id";
        String GROUP_TITLE = "group_title";
        String GROUP_IMAGE = "group_img";
        String GROUP_DESCRIPTION = "group_desc";
        String GROUP_OWNER_ID = "group_owner_id";
        String GROUP_FENCE_ID = "group_fence_id";
        String GROUP_FENCE_NAME = "group_fence_name";
    }

    public static interface Tasks {
        String TABLE_NAME = "Tasks";

        String TASK_ID = "task_id";
        String GROUP_ID = "group_id";
        String TASK_NAME = "task_name";
        String TASK_DESCRIPTION = "task_description";
        String TASK_STATUS = "task_status";
        String TASK_ASSIGN_DATE = "task_assign_date";
    }

    public static interface Fences {
        String TABLE_NAME = "Fences";

        String FENCE_ID = "fence_id";
        String FENCE_NAME = "fence_name";
        String FENCE_LATITUDE = "fence_latitude";
        String FENCE_LONGITUDE = "fence_longitude";
        String FENCE_RADIUS = "fence_radius";
        String FENCE_ID_SERVER = "fence_id_server";

    }
}

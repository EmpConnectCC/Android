package com.example.sibtainraza.empconnectcc.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment1;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment2;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment3;

public class GroupTaskPageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;
    private long groupID;

    public GroupTaskPageAdapter(FragmentManager fm, int NumOfTabs, long id) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.groupID=id;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0: // Assigned
                TaskTabChildFragment1 tab1 = TaskTabChildFragment1.newInstance(groupID);
                return tab1;
            case 1: // In Progress
                TaskTabChildFragment2 tab2 = TaskTabChildFragment2.newInstance(groupID);
                return tab2;
            case 2: // Completed
                TaskTabChildFragment3 tab3 = TaskTabChildFragment3.newInstance(groupID);
                return tab3;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}




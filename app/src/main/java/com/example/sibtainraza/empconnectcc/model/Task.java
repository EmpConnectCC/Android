package com.example.sibtainraza.empconnectcc.model;

/**
 * Created by MuhammadAli on 08-Nov-16.
 */
public class Task {

    private long taskID;
    private int groupID;
    private String taskName;
    private String taskDescription;
    private int taskStatus;
    private String taskAssignDate;

    public Task(){}

    public Task(int taskID, int groupID, String taskName,
                String taskDescription, int taskStatus, String taskAssignDate) {
        this.setTaskID(taskID);
        this.setGroupID(groupID);
//        this.setTaskAssignUserImage(taskAssignUserImage);
        this.setTaskName(taskName);
        this.setTaskDescription(taskDescription);
        this.setTaskStatus(taskStatus);
        this.setTaskAssignDate(taskAssignDate);
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public long getTaskID() {
        return taskID;
    }

    public void setTaskID(long taskID) {
        this.taskID = taskID;
    }

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }

//    public int getTaskAssignUserImage() {
//        return taskAssignUserImage;
//    }

//    public void setTaskAssignUserImage(int taskAssignUserImage) {
//        this.taskAssignUserImage = taskAssignUserImage;
//    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public int getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(int taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskAssignDate() {
        return taskAssignDate;
    }

    public void setTaskAssignDate(String taskAssignDate) {
        this.taskAssignDate = taskAssignDate;
    }

}

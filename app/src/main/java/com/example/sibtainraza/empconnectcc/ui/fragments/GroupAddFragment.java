package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
<<<<<<< HEAD
import android.support.v4.app.FragmentManager;
=======
import android.support.v4.content.ContextCompat;
import android.support.v4.view.accessibility.AccessibilityManagerCompat;
>>>>>>> origin/master
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.UserAdapter;
<<<<<<< HEAD
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
=======
import com.example.sibtainraza.empconnectcc.adapters.UserAddMemberAdapter;
import com.example.sibtainraza.empconnectcc.database.DbScripts;
import com.example.sibtainraza.empconnectcc.model.User;
import com.example.sibtainraza.empconnectcc.utils.AppLog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
>>>>>>> origin/master

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GroupAddFragment.OnGroupAddFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupAddFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupAddFragment extends Fragment implements OnMapReadyCallback {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    Button form1Continue,form2Back,form2Continue,form3Back,form3Save;
    View groupAddFence;

    EditText eTGroupTitle,eTGroupDesc;

    View form1, form2,form3;
    RadioGroup groupAddRadioGroup;
    View rootView;
    ListView listViewGroupMemberAdd;
    ArrayList<User> listUserAddMembers;


    String groupTitle,groupDesc;
    int groupImage;
    long groupFenceID,groupOwnerID;

    private GoogleMap mMap;
    SeekBar seekBar;
    int radiusDB = 50;
    LatLng latLngDB;

    RadioButton groupAddRadioWithLocation,groupAddRadioWithoutLocation;


    private OnGroupAddFragmentInteractionListener mListener;

    private GoogleMap mMap;
    public GroupAddFragment() {
        // Required empty public constructor
    }

    public static GroupAddFragment newInstance(
//            String param1, String param2
    )    {
        GroupAddFragment fragment = new GroupAddFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        rootView =   inflater.inflate(R.layout.fragment_group_add, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        listViewGroupMemberAdd= (ListView)rootView.findViewById(R.id.listViewGroupMembersAdd);

<<<<<<< HEAD
=======
        seekBar = (SeekBar) rootView.findViewById(R.id.seek_bar);



>>>>>>> origin/master
        form1Continue = (Button) rootView.findViewById(R.id.groupAddForm1Continue);
        form2Back = (Button) rootView.findViewById(R.id.groupAddForm2Back);
        form2Continue = (Button) rootView.findViewById(R.id.groupAddForm2Continue);
        form3Back = (Button) rootView.findViewById(R.id.groupAddForm3Back);
        form3Save = (Button) rootView.findViewById(R.id.groupAddForm3Save);

        form1 = (View) rootView.findViewById(R.id.groupAddForm1);
        form2 = (View) rootView.findViewById(R.id.groupAddForm2);
        form3 = (View) rootView.findViewById(R.id.groupAddForm3);


        eTGroupTitle = (EditText) rootView.findViewById(R.id.editText_taskName) ;
        eTGroupDesc = (EditText) rootView.findViewById(R.id.editText_taskDescription) ;

        groupAddRadioGroup = (RadioGroup) rootView.findViewById(R.id.groupAddFenceRadioGroup);

        groupAddRadioWithLocation = (RadioButton) rootView.findViewById(R.id.groupAddRadioBtnWithLocation);
        groupAddRadioWithoutLocation = (RadioButton) rootView.findViewById(R.id.groupAddRadioBtnWithoutLocation);


        groupAddFence = (View) rootView.findViewById(R.id.groupAddFence);


<<<<<<< HEAD
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap = mMap;
            }
        });



=======
>>>>>>> origin/master
        form1Continue.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {


                form1.setVisibility(view.GONE);
                form2.setVisibility(view.VISIBLE);
            }
        });

        form2Continue.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if(groupAddRadioWithLocation.isChecked()){

                    if (latLngDB == null) {
                        Toast.makeText(getContext(), "Select Location", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                form2.setVisibility(view.GONE);
                form3.setVisibility(view.VISIBLE);
            }
        });

        form2Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                form2.setVisibility(view.GONE);
                form1.setVisibility(view.VISIBLE);

            }
        });



////        int sab = groupAddRadioGroup.getCheckedRadioButtonId();
//
//        groupAddRadioGroup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onRadioButtonClicked(view);
//            }
//        });

        groupAddRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.groupAddRadioBtnWithLocation: {
                        groupAddFence.setVisibility(View.VISIBLE);




                        break;
                    }
                    case R.id.groupAddRadioBtnWithoutLocation: {
                        groupAddFence.setVisibility(View.GONE);
                        break;
                    }
                }
            }
        });

<<<<<<< HEAD
        listViewGroupMemberAdd.setAdapter(new UserAdapter(getContext()));
=======

        form3Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                form3.setVisibility(view.GONE);
                form2.setVisibility(view.VISIBLE);
            }
        });


        form3Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getContext(), "Groupp Added", Toast.LENGTH_SHORT).show();

//                    Toast.makeText(getContext(),"Radius : "+radiusDB,Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getContext(),"Lat :"+latLngDB.latitude+"  Lng : "+latLngDB.longitude,Toast.LENGTH_SHORT).show();

//                AppLog.d("Radius","Radius :"+radiusDB );
//                AppLog.d("Latitude","Lat :"+latLngDB.latitude);
//                AppLog.d("Longitude","  Lng : "+latLngDB.longitude );

                String fenceName=null;
                long fenceIdServer = 10;
                long fence_id=-1;
                String address=null;

                if(groupAddRadioWithLocation.isChecked()){

                    Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                    try {
                        List<Address> addresses  = geocoder.getFromLocation(latLngDB.latitude,latLngDB.longitude, 1);


                        address = addresses.get(0).getAddressLine(0);

                        AppLog.d("Address",""+addresses);
                        AppLog.d("Address1",address);



                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    fenceName=address;

                    fence_id = DbScripts.addFence(fenceName,radiusDB, latLngDB.latitude, latLngDB.longitude,fenceIdServer);
                    if (fence_id != -1) {
                        Toast.makeText(getContext(), "fence Added to database :" +fence_id, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "fence NOT Added to database", Toast.LENGTH_SHORT).show();
                    }
                }


                groupTitle= eTGroupTitle.getText().toString().trim();
                groupImage= 1;
                groupDesc= eTGroupDesc.getText().toString().trim();
                groupOwnerID = 12;
                groupFenceID = fence_id;
                String groupFenceName= fenceName;


                long group_id = DbScripts.createGroup(groupTitle,groupImage,groupDesc,groupOwnerID,groupFenceID,groupFenceName);
                if (group_id != -1) {
                    Toast.makeText(getContext(), "group Added to database", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "group NOT Added to database of Group ID"+ group_id, Toast.LENGTH_SHORT).show();
                }



                getFragmentManager().popBackStack();

            }

        });

        listUserAddMembers = DbScripts.getAllUsers();

        listViewGroupMemberAdd.setAdapter(new UserAddMemberAdapter(getContext(),listUserAddMembers));





>>>>>>> origin/master

        return rootView;
    }

    //    public void onRadioButtonClicked(View view) {
//        // Is the button now checked?
//        boolean checked = ((RadioButton) view).isChecked();
//
//        // Check which radio button was clicked
//        switch(view.getId()) {
//            case R.id.groupAddradioBtnWithLocation:
//                if (checked)
//                    view.setVisibility(view.GONE);
//                    break;
//            case R.id.groupAddradioBtnWithoutLocation:
//                if (checked)
//                    view.setVisibility(view.VISIBLE);
//
//                    break;
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupAddFragmentInteractionListener) {
            mListener = (OnGroupAddFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat
                .checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat
                        .checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
//                    Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            return;
        }
        Toast.makeText(getContext(),"Before setMyLocationEnabled",Toast.LENGTH_SHORT).show();
        mMap.setMyLocationEnabled(true);

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(24.9333, 67.0333);

        final Marker marker = mMap.addMarker(new MarkerOptions().position(sydney).title("seydney"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        final Circle circle = mMap.addCircle(new CircleOptions()
                .center(sydney)
                .fillColor(0xaaaaaaaa)
                .radius(radiusDB)
        );


        mMap.animateCamera(CameraUpdateFactory.newLatLng(sydney), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {

            }

            @Override
            public void onCancel() {

            }
        });


        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                latLngDB = latLng;
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
//               markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                marker.setPosition(latLng);
                marker.setTitle(latLng.latitude + " : " + latLng.longitude);
                circle.setCenter(latLng);
                latLngDB = latLng;
            }
        });

        seekBar.setMax(500);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                circle.setRadius(i);
                radiusDB = i;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public interface OnGroupAddFragmentInteractionListener {
        void onGroupAddFragmentInteraction();
    }
}

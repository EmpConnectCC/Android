package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sibtainraza.empconnectcc.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnGroupAddTaskFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupAddTaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupAddTaskFragment extends DialogFragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    EditText taskName, taskDescription;
    Button addTask;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnGroupAddTaskFragmentInteractionListener mListener;

    public GroupAddTaskFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GroupAddTaskFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GroupAddTaskFragment newInstance() {
        GroupAddTaskFragment fragment = new GroupAddTaskFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_group_add_task, container, false);


        taskName = (EditText) rootView.findViewById(R.id.groupSetting_EditText_GroupName);
        taskDescription = (EditText) rootView.findViewById(R.id.groupSetting_EditText_GroupDesc);
        addTask = (Button) rootView.findViewById(R.id.btnAddTask);

        addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Task Added",Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupAddTaskFragmentInteractionListener) {
            mListener = (OnGroupAddTaskFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTaskTabChildFragment1InteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnGroupAddTaskFragmentInteractionListener {
        // TODO: Update argument type and name
        void OnGroupAddTaskFragmentInteractionListener();
    }
}

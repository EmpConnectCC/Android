package com.example.sibtainraza.empconnectcc.model.response;

/**
 * Created by Sibtain Raza on 12/24/2016.
 */
public class UserResp {

    private boolean isValidate;
    private String accessToken;
    private String message;

    public boolean isValidate() {
        return isValidate;
    }

    public void setValidate(boolean validate) {
        isValidate = validate;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

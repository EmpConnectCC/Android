package com.example.sibtainraza.empconnectcc.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sibtainraza.empconnectcc.ui.fragments.GroupMemberTabChildFragment1;
import com.example.sibtainraza.empconnectcc.ui.fragments.GroupMemberTabChildFragment2;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment1;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment2;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment3;

public class MemberPageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public MemberPageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                GroupMemberTabChildFragment1 tab1 = new GroupMemberTabChildFragment1();
                return tab1;
            case 1:
                GroupMemberTabChildFragment2 tab2 = new GroupMemberTabChildFragment2();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}




package com.example.sibtainraza.empconnectcc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.model.User;

import java.util.ArrayList;

/**
 * Created by MuhammadAli on 08-Nov-16.
 */
public class UserAddMemberAdapter extends BaseAdapter {

    ArrayList<User> listViewUser;
    Context context;

    public UserAddMemberAdapter(Context context,ArrayList<User> listViewUser) {
        this.context=context;
        this.listViewUser=listViewUser;

    }

    @Override
    public int getCount() {
        return listViewUser.size();
    }

    @Override
    public Object getItem(int i) {
        return listViewUser.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rootView= inflater.inflate(R.layout.single_item_lv_add_members,viewGroup,false);


        TextView name=(TextView)rootView.findViewById(R.id.textViewGroupMemberName);

        ImageView img= (ImageView)rootView.findViewById(R.id.imageViewUserImage);

        User user= listViewUser.get(i);


        name.setText(user.getFirstName());
//        img.setImageResource(user.getProfileImg());

        return rootView;
    }
}

//package com.example.sibtainraza.empconnectcc.adapters;
//
//import android.app.Activity;
//import android.content.Context;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.example.sibtainraza.empconnectcc.R;
//import com.example.sibtainraza.empconnectcc.model.Group;
//import com.example.sibtainraza.empconnectcc.model.Task;
//import com.example.sibtainraza.empconnectcc.ui.fragments.GroupTaskFragment;
//import com.example.sibtainraza.empconnectcc.utils.AppLog;
//
//import java.util.ArrayList;
//
///**
// * Created by Sibtain Raza on 11/10/2016.
// */
//public class GroupTaskAdapter extends BaseAdapter {
//    private int switchValue; // GETTING VALUE BY WHICH IDENTIFYING WHICH LISTVIEW HAS TO BE REQUEST FROM THE FIREBASE
////    public static final int BLOCKED_MEMBERS = 1, INVITATION_REQUEST = 2, ALL_MEMBERS = 3;
//    public static final int TASK_ASSIGNED = 1, TASK_IN_PROGRESS = 2, TASK_COMPLETED = 3;
//
//    private Context context;
//    private int mResource;
//    private static final String TAG = "GroupTaskAdapter";
//    private Task mTask;
//    private ArrayList<Task> mGroupTaskList;
//    private ArrayList<Task> mGroupTaskListBackup;
//
//    private GroupTaskAdapter groupTaskAdapter;
//
//    TextView taskName, taskDescription;
//    ImageView taskAssignBy;
//    Button popUpButton;
//
////    private ServiceListener<GroupMemberAdapter.GroupMemberData> mAddItemCallBack;
//    private int mCurrentFilterLength;
//
//    public GroupTaskAdapter(Fragment fragment, int layoutResource, Task task, int switchValue, ArrayList<Task> list){
//        this.context = fragment.getContext();
//        this.mResource = layoutResource;
//        this.mTask = task;
//        this.switchValue = switchValue;
//        this.mGroupTaskList = list;
//        this.mGroupTaskListBackup = list;
//        groupTaskAdapter = this;
//        switchValueRequest();
//    }
//
//
//
//
//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup, final Task task) {
//        super(i,view,viewGroup);
//        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
//        View inflate = inflater.inflate(R.layout.single_item_lv_task,viewGroup,false);
//
//        taskName = (TextView) inflate.findViewById(R.id.taskTitle);
//        taskDescription = (TextView) inflate.findViewById(R.id.taskDescription);
//        taskAssignBy = (ImageView) inflate.findViewById(R.id.taskAssignUserImage);
//        popUpButton = (Button) inflate.findViewById(R.id.popUpButton);
//
//        Task task = mGroupTaskList.get(i);
//
//        taskName.setText(task.getTaskName());
//        taskDescription.setText(task.getTaskDescription());
//        taskAssignBy.setImageResource(task.getTaskAssignUserImage());
//
//        return inflate;
//
//    }
//
//
//    private void switchValueRequest() {
//        switch (switchValue) {
//            case GroupTaskAdapter.TASK_ASSIGNED:
//                AppLog.d(TAG + "requestForAllGroupMembers", " request called for TASK_ASSIGNED");
////                GroupService.requestForAllGroupMembers(this);
//                break;
//            case GroupTaskAdapter.TASK_IN_PROGRESS:
//                AppLog.d(TAG + "requestForAllGroupMembers", " request called for TASK_IN_PROGRESS");
////                GroupService.requestForInivitationRequestMembers(this);
//                break;
//            case GroupTaskAdapter.TASK_COMPLETED:
////                todo: firebase request for blocked members
//                AppLog.d(TAG + "requestForAllGroupMembers", " request called for TASK_COMPLETED");
////                GroupService.requestForBlockedMembers(this);
//                break;
//            default:
//                AppLog.d(TAG + "requestForAllGroupMembers", " no request called");
//        }
//    }
//    @Override
//    public int getCount() {
//        return 0;
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return null;
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//
////    public GroupTaskAdapter()
////    public GroupTaskAdapter(Activity c, int layoutResourse, Group group, int switchValue, ArrayList<GroupMemberData> list) {
////        super(FirebaseHandler.getInstance().getGroupsRef(), GroupMemberData.class, layoutResourse, c, list);
////
////        AppLog.v(TAG + "subgroup", "After subgroup constructor start after super");
////        AppLog.d("SwitchValueBaseConstructor1", switchValue + " and " + this.switchValue);
////        this.context = c.getBaseContext();
////        this.mResource = layoutResourse;
////        this.mTask = group;
////        this.switchValue = switchValue;
////        AppLog.d("SwitchValueBaseConstructor2", switchValue + " and " + this.switchValue);
////        firebaseRequest();
////        AppLog.v(TAG + "subgroup", "After subgroup constructor end");
////        this.mMemberDataList = list;
////        this.mMemberDataListBackup = new ArrayList<>(list);
////        AppLog.d("SwitchValueMainAdapterOne", switchValue + "");
////        groupMemberAdapter = this;
////
////
////    public GroupMemberAdapter(Activity c, int layoutResourse, Group group, ArrayList<GroupMemberData> list, int switchValue, ServiceListener<GroupMemberData> addItemCallBack) {
////        this(c, layoutResourse, group, switchValue, list);
////        AppLog.d("SwitchValueMainAdapterTwo", switchValue + "");
////        this.mAddItemCallBack = addItemCallBack;
////    }
////
//
//
//}

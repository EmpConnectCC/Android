package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.ChatAdapter;
import com.example.sibtainraza.empconnectcc.adapters.UserAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnGroupMemberFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupMemberFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupMemberFragment extends Fragment {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ListView listView;

    private OnGroupMemberFragmentInteractionListener mListener;

    public GroupMemberFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GroupMemberFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GroupMemberFragment newInstance(String param1, String param2) {
        GroupMemberFragment fragment = new GroupMemberFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       rootView=inflater.inflate(R.layout.fragment_group_member, container, false);

        initViews();
        setUpAdapter();



        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.OnTaskTabChildFragment1InteractionListener(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupMemberFragmentInteractionListener) {
            mListener = (OnGroupMemberFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupMemberFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void initViews() {

        listView = (ListView)rootView.findViewById(R.id.list_Group_Menmbers);
    }

    private void setUpAdapter(){
        listView.setAdapter(new UserAdapter(getContext()));

    }



    public static GroupMemberFragment newInstance() {
        GroupMemberFragment fragment = new GroupMemberFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnGroupMemberFragmentInteractionListener {
        // TODO: Update argument type and name
        void OnGroupMemberFragmentInteractionListener();
    }
}

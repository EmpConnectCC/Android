package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.GroupTaskPageAdapter;
import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.utils.AppLog;

public class GroupTaskFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Group groupObj;
//    ListView listViewAssignedTask;
    TabLayout grouptaskTabLayout;
    ViewPager groupTaskViewPager;

    private OnGroupTaskFragmentInteractionListener mListener;
    android.support.v4.view.PagerAdapter userAdapter;

    public GroupTaskFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GroupTaskFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GroupTaskFragment newInstance(Group group) {
        GroupTaskFragment fragment = new GroupTaskFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1,group);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        groupObj = getArguments().getParcelable(ARG_PARAM1);
        AppLog.d("GroupObject - ",groupObj.getGroupID()+"");
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_task, container, false);



        initViews();
        setUpAdapter();



        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupTaskFragmentInteractionListener) {
            mListener = (OnGroupTaskFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupMemberFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initViews() {
        grouptaskTabLayout = (TabLayout) rootView.findViewById(R.id.groupTaskToolbar);
        grouptaskTabLayout.addTab(grouptaskTabLayout.newTab().setText("Assigned"));
        grouptaskTabLayout.addTab(grouptaskTabLayout.newTab().setText("In Progress"));
        grouptaskTabLayout.addTab(grouptaskTabLayout.newTab().setText("Completed"));
        groupTaskViewPager = (ViewPager) rootView.findViewById(R.id.groupTaskPager);

    }

    private void setUpAdapter(){
       userAdapter =
        new GroupTaskPageAdapter(getFragmentManager(),grouptaskTabLayout.getTabCount(),groupObj.getGroupID());

        groupTaskViewPager.setAdapter(userAdapter);
        groupTaskViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(grouptaskTabLayout));
//        groupTaskViewPager.setOffscreenPageLimit(2);

        grouptaskTabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        grouptaskTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                groupTaskViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    public interface OnGroupTaskFragmentInteractionListener {
        void OnGroupTaskFragmentInteractionListener(int constant, @Nullable Object o);
    }
}

package com.example.sibtainraza.empconnectcc.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.model.Group;
import com.example.sibtainraza.empconnectcc.ui.fragments.ChatFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.ChatFragmentTab1Users;
import com.example.sibtainraza.empconnectcc.ui.fragments.ChatFragmentTab2Chats;
import com.example.sibtainraza.empconnectcc.ui.fragments.ChatUiFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.GroupAddFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.GroupAddTaskFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.GroupHomeFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.GroupListFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.GroupMemberFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.GroupTaskFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.NavigationDrawerFragment;
import com.example.sibtainraza.empconnectcc.ui.fragments.TaskTabChildFragment1;

/**
 * Created by Sibtain Raza on 10/17/2016.
 */
public class HomeActivity extends AppCompatActivity implements
        GroupListFragment.OnGroupListFragmentInteractionListener,
        GroupHomeFragment.OnGroupHomeFragmentInteractionListener,
        GroupAddFragment.OnGroupAddFragmentInteractionListener,
        GroupMemberFragment.OnGroupMemberFragmentInteractionListener,
        ChatFragment.OnChatFragmentInteractionListener,
        GroupTaskFragment.OnGroupTaskFragmentInteractionListener,
        TaskTabChildFragment1.OnTaskTabChildFragment1InteractionListener,
        ChatFragmentTab1Users.OnChatFragmentUsersInteractionListener,
        ChatFragmentTab2Chats.OnChatChatsFragmentInteractionListener,
        ChatUiFragment.OnChatUiFragmentInteractionListener,
        NavigationDrawerFragment.OnNavigationDrawerFragmentInteractionListener {

    private FragmentManager fragmentManager;

    private ImageView userProfileImage;
    private TextView userName, userEmail;
    private ListView userCheckInGroupsLW;
    private Button checkInOut, logOut;
    private NavigationDrawerFragment navigationDrawerFragment;

    private DrawerLayout drawerLayout;

    private static final String TAG = "HomeActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("TAG", "in create , are you serious");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        ViewTreeObserver viewTreeObserver = toolbar.getViewTreeObserver();
//        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
//
//            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    LinearLayout drawerLeft = (LinearLayout) findViewById(R.id.left_drawer);
//                    RelativeLayout leftSide = (RelativeLayout) drawerLeft.findViewWithTag("left_side");
//                    RelativeLayout filterHeading = (RelativeLayout) leftSide.findViewWithTag("filterHeading");
//                    filterHeading.getLayoutParams().height = toolbar.getHeight();
//                }
//            });
//        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        navigationDrawerFragment = new NavigationDrawerFragment();

        fragmentManager = getSupportFragmentManager();
        replaceFragment(new GroupListFragment(), false);


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frameNavigationDrawer, navigationDrawerFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frameHomeActivity, fragment);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }


    @Override
    public void onGroupListFragmentInteraction(int constant, @Nullable Object o) {
        switch (constant) {
            case ADD_GROUP:
                replaceFragment(GroupAddFragment.newInstance(), true);
                break;
            case GROUP_HOME:
                replaceFragment(GroupHomeFragment.newInstance((Group)o), true);
                break;
        }
    }

    @Override
    public void onGroupHomeFragmentInteraction(int constant, @Nullable Object o) {
        switch (constant) {
            case GROUP_MEMBER:
                replaceFragment(GroupMemberFragment.newInstance(), true);
                break;
            case GROUP_SETTING:
//                replaceFragment(GroupSettingFragment.newInstance(),true);
                Intent intent = new Intent(this, GroupSettingActivity.class);
                startActivity(intent);
                break;
            case GROUP_CHAT:
                replaceFragment(ChatFragment.newInstance(), true);
                break;
            case GROUP_TASK:
                replaceFragment(GroupTaskFragment.newInstance((Group)o), true);
                break;
        }
    }

    @Override
    public void onGroupAddFragmentInteraction() {

    }

    @Override
    public void OnGroupMemberFragmentInteractionListener() {

    }


    @Override
    public void OnChatFragmentInteractionListener() {

    }


    @Override
    public void OnGroupTaskFragmentInteractionListener(int constant, @Nullable Object o) {

    }

    @Override
    public void OnTaskTabChildFragment1InteractionListener(int constant, @Nullable Object o) {
        switch (constant) {
            case ADD_TASK:
                replaceFragment(GroupAddTaskFragment.newInstance(), true);
                break;
        }
    }

    @Override
    public void onChatChatsFragmentInteraction() {

    }

    @Override
    public void onChatUsersFragmentInteraction(int constant, @Nullable Object o) {

        switch (constant) {
            case CHAT:
                replaceFragment(ChatUiFragment.newInstance(),true);
                break;
        }

    }

    @Override
    public void onChatUiFragmentInteraction() {

    }

    @Override
    public void OnNavigationDrawerFragmentInteraction(int constant, @Nullable Object o) {
        switch (constant){
            case LOGOUT:
                startActivity(new Intent(this,LoginActivity.class));
                finish();
                break;
        }
    }
}

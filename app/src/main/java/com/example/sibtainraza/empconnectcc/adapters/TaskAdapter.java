package com.example.sibtainraza.empconnectcc.adapters;

/**
 * Created by MuhammadAli on 08-Nov-16.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.database.DbScripts;
import com.example.sibtainraza.empconnectcc.model.Task;

import java.util.ArrayList;

public class TaskAdapter extends BaseAdapter {

    Context context;

    TextView taskName, taskDescription;
    ImageView taskAssignBy;
    Button popUpButton;

    ArrayList<Task> listViewTask = new ArrayList<Task>();;

    public TaskAdapter(Context context, ArrayList<Task> listViewTask) {
        this.context = context;
        this.listViewTask = listViewTask;
    }

    @Override
    public int getCount() {
        return listViewTask.size();
    }

    @Override
    public Object getItem(int i) {
        return listViewTask.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View inflate = inflater.inflate(R.layout.single_item_lv_task,viewGroup,false);

        taskName = (TextView) inflate.findViewById(R.id.taskTitle);
        taskDescription = (TextView) inflate.findViewById(R.id.taskDescription);
        taskAssignBy = (ImageView) inflate.findViewById(R.id.taskAssignUserImage);
        popUpButton = (Button) inflate.findViewById(R.id.popUpButton);

        Task task = listViewTask.get(i);

        setTask(task);

        return inflate;
    }

    public void setTask(Task task) {
        taskName.setText(task.getTaskName());
        taskDescription.setText(task.getTaskDescription());
    }
}
package com.example.sibtainraza.empconnectcc;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import com.example.sibtainraza.empconnectcc.ui.activities.LoginActivity;
import com.example.sibtainraza.empconnectcc.utils.AppLog;

public class MyApplication extends Application {

    private static final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication instance;

    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppLog.i(TAG,"onCreate");
        instance = this;
    }

    public static boolean checkPermissionWriteExternal(){
        int writePerm;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            try {
                writePerm = ContextCompat.checkSelfPermission(MyApplication.getInstance(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            } catch (Exception e){
                e.printStackTrace();
                return false;
            }
            return writePerm == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }
}

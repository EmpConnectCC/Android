package com.example.sibtainraza.empconnectcc.ui.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.services.cores.cores.MenuManagerService;

public class GroupSettingActivity extends AppCompatActivity {

    android.support.v4.app.FragmentManager fragmentManager;

    ViewPager viewPager;
    private MenuManagerService menuManagerService;
//    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_setting);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((AppCompatActivity)this).setSupportActionBar(toolbar);
//        toolbar.setLogo(R.drawable.app_icon);
        menuManagerService = new MenuManagerService();
        ((AppCompatActivity)this).getSupportActionBar().setTitle("");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.groupSetting_TabLayout);
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.group_setting_tab_groups));
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.group_setting_tab_users));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.groupSetting_ViewPager);
        PagerAdapter adapter = new com.example.sibtainraza.empconnectcc.adapters.PagerAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//    }


}

package com.example.sibtainraza.empconnectcc.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.sibtainraza.empconnectcc.MyApplication;

/**
 * Created by Sibtain Raza on 12/24/2016.
 */

public class SharedPreferenceManager {

    public static final String TAG = SharedPreferenceManager.class.getSimpleName();
    private SharedPreferences preferences;
    private static SharedPreferenceManager manager;

    static {
        getManager();
    }

    public static SharedPreferenceManager getManager() {
        if (manager == null) {
            manager = new SharedPreferenceManager();
        }
        return manager;
    }

    public SharedPreferenceManager() {
        preferences = MyApplication.getInstance().getSharedPreferences("APP", Context.MODE_PRIVATE);
    }

    public String getUserAuth() {
        String auth = preferences.getString(Constants.USER_AUTH, null);
        AppLog.i(TAG,"getAuth --- " + auth);
        return auth;
    }

    public void setCheckedInFenceId(int checkedInId) {
        preferences.edit().putInt(Constants.CHECKDIN_FENCE_ID, checkedInId).apply();
    }

    public int getCheckedInFenceId() {
        int checkedInId = preferences.getInt(Constants.CHECKDIN_FENCE_ID, -1);
        AppLog.i(TAG,"getCheckedInId --- " + checkedInId);
        return checkedInId;
    }

    public void setCheckedInStatus(boolean checkedInStatus) {
        preferences.edit().putBoolean(Constants.CHECKEDIN_STATUS, checkedInStatus).apply();
    }
    public boolean getCheckedInStatus() {
        boolean checkInFenceId = preferences.getBoolean(Constants.CHECKEDIN_STATUS, false);
        AppLog.i(TAG,"getCheckedInFenceId --- " + checkInFenceId);
        return checkInFenceId;
    }

    public void setUserAuth(String auth) {
        preferences.edit().putString(Constants.USER_AUTH, auth).apply();
    }

    interface Constants {
        String USER_AUTH = "USER_AUTH";
        String CHECKEDIN_STATUS ="CHECKEDIN_STATUS";
        String CHECKDIN_FENCE_ID = "CHECKDIN_FENCE_ID";
    }
}

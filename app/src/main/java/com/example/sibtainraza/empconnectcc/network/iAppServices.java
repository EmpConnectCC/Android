package com.example.sibtainraza.empconnectcc.network;

import com.example.sibtainraza.empconnectcc.model.SignIn;
import com.example.sibtainraza.empconnectcc.model.User;
import com.example.sibtainraza.empconnectcc.model.response.UserResp;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by asultan on 8/21/2015.
 */
public interface iAppServices {
//String[] HEADER = {"client_id: 4a968212", "client_secret: da402a3935cb5ef21159ce8893b23c5d", "token: ze+WSDjy8hAEmif4vh8leevzH/H2ylFuOvBFFQj2tz8ViaX0OVYEfi+buJ1ONxMIl3AkvFsoXTk="};

//    @GET("/discovers/events")
//    void discoverEvents(@Query("latitude") String latitude, @Query("longitude") String longitude, @Query("ratio") String ratio, Callback<EventList> callback);

    @POST("/user/user")
    Call<UserResp> loginUser(@Body SignIn signIn);

}
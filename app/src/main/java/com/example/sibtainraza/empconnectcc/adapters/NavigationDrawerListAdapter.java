package com.example.sibtainraza.empconnectcc.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.model.Group;

import java.util.ArrayList;

/**
 * Created by MuhammadAli on 30-Dec-16.
 */
public class NavigationDrawerListAdapter extends BaseAdapter {


    SharedPreferences pref;
    SharedPreferences.Editor editor;


    ArrayList<Group> list;
    Context context;



    public NavigationDrawerListAdapter(Context context, ArrayList<Group> list) {

        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.single_row_navigation_drawer_list, viewGroup, false);

        pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();


        TextView groupName = (TextView) row.findViewById(R.id.groupName);
        TextView fenceName = (TextView) row.findViewById(R.id.fenceName);


        Group temp = list.get(i);


        groupName.setText(temp.getGroupTitle());
        fenceName.setText(temp.getGroupFenceName());

        //////////////////////////////////////////////////////////////
        return row;
    }

}

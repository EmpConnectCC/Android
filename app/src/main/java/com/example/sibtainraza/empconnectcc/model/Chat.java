package com.example.sibtainraza.empconnectcc.model;

/**
 * Created by MuhammadAli on 08-Nov-16.
 */
public class Chat {

    private String chatName;
    private String chatMessage;
    private int chatImg;

    public Chat(String chatName, String chatMessage, int chatImg) {
        this.chatName = chatName;
        this.chatMessage = chatMessage;
        this.chatImg = chatImg;
    }

    public String getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(String chatMessage) {
        this.chatMessage = chatMessage;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public int getChatImg() {
        return chatImg;
    }

    public void setChatImg(int chatImg) {
        this.chatImg = chatImg;
    }




}

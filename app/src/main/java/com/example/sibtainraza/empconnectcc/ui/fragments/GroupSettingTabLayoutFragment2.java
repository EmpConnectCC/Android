package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.adapters.MemberPageAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnGroupSettingMemberTabChildFragment2InteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupSettingTabLayoutFragment2#newInstance} factory method to
 * create an instance of this fragment.
 */

public class GroupSettingTabLayoutFragment2 extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    View rootView;
    ListView listView;

    TabLayout groupSettingTabLayout;
    ViewPager groupSettingViewPager;
    MemberPageAdapter memberPageAdapter;

    public GroupSettingTabLayoutFragment2() {
    }

    public static GroupSettingTabLayoutFragment2 newInstance(String param1, String param2) {
        GroupSettingTabLayoutFragment2 fragment = new GroupSettingTabLayoutFragment2();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1); //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        //mParam1 = getArguments().getString(ARG_PARAM1); //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.group_setting_tab_child_fragment_2, container, false);

        initViews();
        setUpAdapter();

        return rootView;
    }

    private void initViews() {
        groupSettingTabLayout = (TabLayout) rootView.findViewById(R.id.groupSetting_Tab2_TabLayout);
        groupSettingTabLayout.addTab(groupSettingTabLayout.newTab().setText("All Members"));
        groupSettingTabLayout.addTab(groupSettingTabLayout.newTab().setText("Group Members"));
        groupSettingViewPager = (ViewPager) rootView.findViewById(R.id.groupSetting_Tab2_ViewPager);
    }

    private void setUpAdapter() {
        memberPageAdapter =
                new MemberPageAdapter(getFragmentManager(),groupSettingTabLayout.getTabCount());


        groupSettingViewPager.setAdapter(memberPageAdapter);
        groupSettingViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(groupSettingTabLayout));
//        groupTaskViewPager.setOffscreenPageLimit(2);

        groupSettingTabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        groupSettingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                groupSettingViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnGroupSettingMemberTabChildFragment2InteractionListener) {
//            mListener = (OnGroupSettingMemberTabChildFragment2InteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement GroupSettingMemberTabChildFragment1InteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}

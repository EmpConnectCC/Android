package com.example.sibtainraza.empconnectcc.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sibtain Raza on 11/5/2016.
 */
public class Group implements Parcelable{

    private long groupID;
    private String groupTitle;
    private int groupImage;
    private String groupDesc;
    private int groupOwnerID;
    private long groupFenceID;
    private String groupFenceName;

    // Final------------------

    public Group(){}

    public Group(long groupID, String groupTitle, int groupImage, String groupDesc,
                 int groupOwnerID, int groupFenceID, String groupFenceName) {
        this.setGroupID(groupID);
        this.setGroupTitle(groupTitle);
        this.setGroupImage(groupImage);
        this.setGroupDesc(groupDesc);
        this.setGroupOwnerID(groupOwnerID);
        this.setGroupFenceID(groupFenceID);
        this.setGroupFenceName(groupFenceName);
    }

    protected Group(Parcel in) {
        groupID = in.readLong();
        groupTitle = in.readString();
        groupImage = in.readInt();
        groupDesc = in.readString();
        groupOwnerID = in.readInt();
        groupFenceID = in.readLong();
        groupFenceName = in.readString();
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public int getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(int groupImage) {
        this.groupImage = groupImage;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public int getGroupOwnerID() {
        return groupOwnerID;
    }

    public void setGroupOwnerID(int groupOwnerID) {
        this.groupOwnerID = groupOwnerID;
    }

    public long getGroupFenceID() {
        return groupFenceID;
    }

    public void setGroupFenceID(long groupFenceID) {
        this.groupFenceID = groupFenceID;
    }

    public String getGroupFenceName() {
        return groupFenceName;
    }

    public void setGroupFenceName(String groupFenceName) {
        this.groupFenceName = groupFenceName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(groupID);
        parcel.writeString(groupTitle);
        parcel.writeInt(groupImage);
        parcel.writeString(groupDesc);
        parcel.writeInt(groupOwnerID);
        parcel.writeLong(groupFenceID);
        parcel.writeString(groupFenceName);
    }
}
package com.example.sibtainraza.empconnectcc.fencing;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingApi;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataApi;

import java.util.ArrayList;

public class FencingHandler {

    private static final String TAG = FencingHandler.class.getSimpleName();

    ArrayList<Geofence> geoFenceList = new ArrayList<>();

    PendingIntent geoFencePendingIntent;

    Context context;

    public FencingHandler(Context context, ArrayList<FenceInfo> fences) {

        geoFenceList.clear();                                     //for refrensing geo fences list
        for (FenceInfo fence : fences) {
            addFenceToList(fence);
        }

        this.context = context;

        //establish connection to google api which is mandatory for any communication with google api
        establishGoogleApiConnection();
    }

    private void establishGoogleApiConnection() {
        GoogleApiHandler.getInstance(context).getGoogleApiClient().connect();
        GoogleApiHandler.getInstance(context).getGoogleApiClient()
                .registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {

                        if (geoFenceList.size() == 0){
                        removeGeofencesHandler();
                        return;
                        }
                        removeGeofencesHandler();
                        registerFences();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                });
    }

    private void addFenceToList(FenceInfo fence) {
        Log.d(TAG, "adding fence = " + fence.id);
        Log.d(TAG, "adding fence = " + fence.radius);
        Log.d(TAG, "adding fence = " + fence.latitude);
        Log.d(TAG, "adding fence = " + fence.longitude);


        geoFenceList.add(new Geofence.Builder()
                .setRequestId(fence.id +"")
                .setCircularRegion(
                        fence.latitude,
                        fence.longitude,
                        fence.radius
                )
                .setExpirationDuration(1000 * 60 * 60 * 24 * 20)  //20 days

                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_DWELL | Geofence.GEOFENCE_TRANSITION_EXIT)
                //Sets the delay between GEOFENCE_TRANSITION_ENTER and GEOFENCE_TRANSITION_DWELLING in milliseconds.
                .setLoiteringDelay(1000 * 5)
                .build());
    }

    //fencing request that will be registered for registering fences
    private GeofencingRequest getGeofencingRequest() {
        Log.d(TAG, "getGeofencingRequest --- fences count = " + geoFenceList.size());
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        //mention triggers that this request will respond to
        builder.setInitialTrigger(
                GeofencingRequest.INITIAL_TRIGGER_EXIT | GeofencingRequest.INITIAL_TRIGGER_DWELL
        );

        //add the geofences list
        builder.addGeofences(geoFenceList);
        return builder.build();
    }

    public PendingIntent getGeoFencePendingIntent() {
        Log.d(TAG, "getGeoFencePendingIntent");
        // Reuse the PendingIntent if we already have it.
        if (geoFencePendingIntent != null) {
            return geoFencePendingIntent;
        }
        Intent intent;
        intent = new Intent(context, GeofenceTransitionsIntentService.class);       // ye kia hora he ???
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences()
        return PendingIntent.getService(context, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }

    public void registerFences() {
        Log.d(TAG, "registerFences");
        if (!GoogleApiHandler.getInstance(context).getGoogleApiClient().isConnected()) {
            Log.e(TAG, "registerFences --- GoogleApiClient not connected");
            return;
        }
        Log.i(TAG, "registering geofences");

        //for marshmallo runtime permission check for accessing loaction
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Permission not granted for location", Toast.LENGTH_SHORT).show();
            return;
        }
        if (geoFenceList.size() == 0) {
            Log.d(TAG, "no fence added");
            Toast.makeText(context, "No fences to be added", Toast.LENGTH_SHORT).show();

            return;
        }
        LocationServices.GeofencingApi.addGeofences(
                GoogleApiHandler.getInstance(context).getGoogleApiClient(),
                getGeofencingRequest(),
                getGeoFencePendingIntent()
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                Log.i(TAG, "ResultCallback --- onResult: " + status.toString());
            }
        });
    }


    public void removeGeofencesHandler() {
        if (!GoogleApiHandler.getInstance(context).getGoogleApiClient().isConnected()) {
            //  Toast.makeText(this, getString(R.string.not_connected), Toast.LENGTH_SHORT).show();
            return;
        }

        // Remove geofences.
        LocationServices.GeofencingApi.removeGeofences(
                GoogleApiHandler.getInstance(context).getGoogleApiClient(),
                getGeoFencePendingIntent()
                // This is the same pending intent that was used in addGeofences().
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Log.i(TAG, "ResultCallback --- onResult: " + status.toString());

            }
        }); // Result processed in onResult().

    }


    private String getErrorString(int errorCode) {
        switch (errorCode) {
            case GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE:
                return "geofence_not_available";
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES:
                return "GEOFENCE_TOO_MANY_GEOFENCES";
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS:
                return "GEOFENCE_TOO_MANY_PENDING_INTENTS";
            default:
                return "unknown_geofence_error";
        }
    }

    public static class FenceInfo {
        //required for identifying fence
        //if using firebase then you can use the push id(id of data in array) as this id

        public int id;
        public double latitude, longitude;
        public int radius;

        @Override
        public String toString() {
            return "id = " + id + " --- radius = " + radius + " --- lat = " + latitude + " --- lng = " + longitude;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public int getRadius() {
            return radius;
        }

        public void setRadius(int radius) {
            this.radius = radius;
        }
    }

}
package com.example.sibtainraza.empconnectcc.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.sibtainraza.empconnectcc.R;
import com.example.sibtainraza.empconnectcc.model.Group;

import java.util.ArrayList;

/**
 * Created by Sibtain Raza on 11/5/2016.
 */
public class HomeFragmentCardAdapter extends RecyclerView.Adapter<HomeFragmentCardAdapter.HomeViewHolder> {
    private Context mContext;

    ArrayList<Group> listViewGroup = new ArrayList<Group>();

    @Nullable
    private ItemSelectedListener itemSelectedListener;

    public HomeFragmentCardAdapter(ArrayList<Group> listViewGroup){
        this.listViewGroup = listViewGroup;
    }

    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_layout,parent,false);
        HomeViewHolder homeViewHolder = new HomeViewHolder(view);
        return homeViewHolder;
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, int position) {
        final Group grp = listViewGroup.get(position);

        holder.groupTitleTextView.setText(grp.getGroupTitle());
//        holder.groupImageView.setImageResource(grp.getGroupImage());
        holder.groupFenceName.setText(grp.getGroupFenceName());
//      holder.groupOwnerImageView.setImageResource(grp.getGroupOwnerID());

        holder.groupCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemSelectedListener != null){
                    itemSelectedListener.onItemSelected(grp);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listViewGroup.size();
    }


    public void setItemSelectedListener(@Nullable ItemSelectedListener itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder{

        TextView groupTitleTextView;
        TextView groupFenceName;
        ImageView groupImageView;
        ImageView groupOwnerImageView;

        RelativeLayout groupCardView;

        public HomeViewHolder(View itemView) {
            super(itemView);
            groupTitleTextView = (TextView) itemView.findViewById(R.id.groupTitleTextView);
            groupFenceName = (TextView) itemView.findViewById(R.id.groupPhysicalLocation);
            groupImageView = (ImageView) itemView.findViewById(R.id.groupLogo);
            groupOwnerImageView = (ImageView) itemView.findViewById(R.id.circleView);

            groupCardView = (RelativeLayout) itemView.findViewById(R.id.cardView);
        }
    }

    public interface ItemSelectedListener{
        public void onItemSelected(Group group);
    }
}

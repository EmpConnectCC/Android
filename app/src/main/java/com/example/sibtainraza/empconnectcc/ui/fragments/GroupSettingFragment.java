package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.example.sibtainraza.empconnectcc.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnGroupSettingFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupSettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupSettingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Button formAddGroupContinue,formAddGroupBack;
    View form1, form2;
    View rootView;

    private OnGroupSettingFragmentInteractionListener mListener;

    public GroupSettingFragment() {
        // Required empty public constructor
    }

    public static GroupSettingFragment newInstance(){
        GroupSettingFragment fragment = new GroupSettingFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =    inflater.inflate(R.layout.fragment_group_setting, container, false);


        formAddGroupContinue = (Button) rootView.findViewById(R.id.profileForm1_Continue);
        formAddGroupBack = (Button) rootView.findViewById(R.id.profileForm2_back);
        form1 = (View) rootView.findViewById(R.id.form1_profileSetting);
        form2 = (View) rootView.findViewById(R.id.form2_profileSetting);
        formAddGroupContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                form1.setVisibility(View.GONE);
                form2.setVisibility(View.VISIBLE);
                Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right);
                form2.startAnimation(animation);
            }
        });

        formAddGroupBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                form2.setVisibility(View.GONE);
                form1.setVisibility(View.VISIBLE);
                Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_left);
                form1.startAnimation(animation);
            }
        });


//        formAddGroupBack.setOnClickListener();

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.OnTaskTabChildFragment1InteractionListener(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupSettingFragmentInteractionListener) {
            mListener = (OnGroupSettingFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }




    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnGroupSettingFragmentInteractionListener {
        // TODO: Update argument type and name
        void OnGroupSettingFragmentInteractionListener();
    }
}

package com.example.sibtainraza.empconnectcc.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sibtainraza.empconnectcc.ui.fragments.GroupSettingTabLayoutFragment1;
import com.example.sibtainraza.empconnectcc.ui.fragments.GroupSettingTabLayoutFragment2;

/**
 * Created by panacloud on 6/27/15.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                GroupSettingTabLayoutFragment1 tab1 = new GroupSettingTabLayoutFragment1();
                return tab1;
            case 1:
                GroupSettingTabLayoutFragment2 tab3 = new GroupSettingTabLayoutFragment2();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

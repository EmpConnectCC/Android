package com.example.sibtainraza.empconnectcc.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.sibtainraza.empconnectcc.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GroupSettingMemberTabChildFragment1InteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupSettingTabLayoutFragment1#newInstance} factory method to
 * create an instance of this fragment.
 */

public class GroupSettingTabLayoutFragment1 extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    Button form1Continue,form2Back,form2Save;
    View form1, form2, groupSettingFence;
    RadioGroup groupSettingRadioGroup;


    View rootView;

    public GroupSettingTabLayoutFragment1() {
    }

    public static GroupSettingTabLayoutFragment1 newInstance(String param1, String param2) {
        GroupSettingTabLayoutFragment1 fragment = new GroupSettingTabLayoutFragment1();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1); //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        //mParam1 = getArguments().getString(ARG_PARAM1); //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.group_setting_tab_child_fragment_1, container, false);

        form1Continue = (Button) rootView.findViewById(R.id.groupSettingForm1Continue);
        form2Back = (Button) rootView.findViewById(R.id.groupSettingForm2Back);
        form2Save = (Button) rootView.findViewById(R.id.groupSettingForm2Save);
        groupSettingRadioGroup = (RadioGroup) rootView.findViewById(R.id.groupSettingFenceRadioGroup);
        groupSettingFence = (View) rootView.findViewById(R.id.groupSettingFence);

        form1 = (View) rootView.findViewById(R.id.groupSettingForm1);
        form2 = (View) rootView.findViewById(R.id.groupSettingForm2);
        form1Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                form1.setVisibility(view.GONE);
                form2.setVisibility(view.VISIBLE);
            }
        });

        form2Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                form2.setVisibility(view.GONE);
                form1.setVisibility(View.VISIBLE);
            }
        });

        form2Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Group Setting Updated", Toast.LENGTH_SHORT).show();
            }
        });

        groupSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.settingFenceRadioBtnWithLocation: {
                        groupSettingFence.setVisibility(View.VISIBLE);
                        break;
                    }
                    case R.id.settingFenceRadioBtnWithoutLocation: {
                        groupSettingFence.setVisibility(View.GONE);
                        break;
                    }
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof GroupSettingMemberTabChildFragment1InteractionListener) {
//            mListener = (GroupSettingMemberTabChildFragment1InteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement GroupSettingMemberTabChildFragment1InteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
